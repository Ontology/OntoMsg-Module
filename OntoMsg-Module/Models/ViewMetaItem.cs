﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoMsg_Module.Models
{
    public class ViewMetaItem
    {
        public string IdView { get; set; }
        public string NameView { get; set; }
        public string IdController { get; set; }
        public string NameController { get; set; }
        public string IdModule { get; set; }
        public string NameModule { get; set; }
        public string IdCommandLineRun { get; set; }
        public string NameCommandLineRun { get; set; }
        public List<clsOntologyItem> ModuleFunctions { get; set; }

        public bool IsRelatedToFunction(string functionId)
        {
            return ModuleFunctions.Any(moduleFunction => moduleFunction.GUID == functionId);
        }

        public ViewMetaItem()
        {
            ModuleFunctions = new List<clsOntologyItem>();
        }
    }
}
