﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoMsg_Module.Models
{
    public class ResultItem<T>
    {
        public clsOntologyItem ResultState { get; set; }
        public T Result { get; set; }
    }
}
