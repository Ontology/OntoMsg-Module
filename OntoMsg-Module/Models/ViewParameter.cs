﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoMsg_Module.Models
{
    public enum StandardParameter
    {
        None = 0,
        Sender = 1,
        Object = 2,
        Class = 3,
        RelationType = 4,
        AttributeType = 5,
        View = 6
    }
    public class ViewParameter
    {
        public string Key { get; private set; }
        public string Value { get; private set; }
        public bool Valid { get; private set; }
        public StandardParameter StandardParameter { get; private set; }

        public bool KeyFound(string key)
        {
            if (Key != null && Key.ToLower() == key.ToLower())
            {
                return true;
            }

            return false;
        }

        public ViewParameter(string keyValueParam)
        {
            var splitted = keyValueParam.Split('=');

            if (splitted.Count() != 2)
            {
                Valid = false;
                return;
            }

            Key = splitted[0];
            Value = splitted[1];

            if (Key.ToLower() == "Sender".ToLower())
            {
                StandardParameter = StandardParameter.Sender;
            }
            else if (Key.ToLower() == "Object".ToLower())
            {
                StandardParameter = StandardParameter.Object;
            }
            else if (Key.ToLower() == "Class".ToLower())
            {
                StandardParameter = StandardParameter.Class;
            }
            else if (Key.ToLower() == "RelationType".ToLower())
            {
                StandardParameter = StandardParameter.RelationType;
            }
            else if (Key.ToLower() == "AttributeType".ToLower())
            {
                StandardParameter = StandardParameter.AttributeType;
            }
            else if (Key.ToLower() == "View".ToLower())
            {
                StandardParameter = StandardParameter.View;
            }
        }
    }
}
