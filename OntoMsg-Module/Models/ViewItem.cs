﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoMsg_Module.Models
{
    public class ViewItem
    {
        public string IdViewInstance { get; set; }
        public string ViewItemId { get; set; }
        public string ViewItemClass { get; set; }
        public string ViewItemType { get; set; }
        public object ViewItemValue { get; set; }
        public string ActionChangeValue { get; set; }
        public List<ViewItemValue> ViewItemValues { get; set; } = new List<Models.ViewItemValue>();
        public ViewItemValue LastValue { get; set; }
        public string SpecialType { get; set; }
        
        private object locker = new object();

        public void AddValue(object value)
        {
            lock (locker)
            {
                ViewItemValues.Add(new ViewItemValue { Value = value });
            }
        }

        public void ClearValues()
        {
            lock (locker)
            {
                ViewItemValues.Clear();
            }
        }

        public bool HasChanged
        {
            get
            {
                var result = false;
                lock (locker)
                {
                    result = ViewItemValues != null && ViewItemValues.Any();
                }

                return result;
            }
        }


        public ViewItemValue ChangeViewItemValue(string viewItemType, object value)
        {
            var viewItemValue = ViewItemValues.FirstOrDefault(viewItmVal => viewItmVal.ViewItemType == viewItemType);
            if (viewItemValue == null)
            {
                viewItemValue = new ViewItemValue
                {
                    ViewItemType = viewItemType
                };
                ViewItemValues.Add(viewItemValue);
            }

            
            viewItemValue.Value = value;
            LastValue = viewItemValue;
            return viewItemValue;
        }

        public ViewItemValue GetViewItemValue(string viewItemType)
        {
            var viewItemValue = ViewItemValues.FirstOrDefault(viewItmVal => viewItmVal.ViewItemType == viewItemType);
            return viewItemValue;
        }
        

        public ViewItem()
        {
            ViewItemValues = new List<ViewItemValue>();
        }
    }

    public class ViewItemValue
    {
        public string ViewItemType { get; set; }
        public object Value { get; set; }
    }
}
