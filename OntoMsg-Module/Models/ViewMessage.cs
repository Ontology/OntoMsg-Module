﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoMsg_Module.Models
{
    [Flags]
    public enum ViewMessageType
    {
        Unknown = 0,
        OK = 1,
        Exclamation = 2,
        Information = 4,
        Error = 8,
        Critical = 16,
        Note = 32
    }
    public class ViewMessage
    {
        public string Message { get; set; }
        public string Short { get; set; }
        public ViewMessageType FormMessageType { get; set; }

        public ViewMessage(string message, string shortMessage, ViewMessageType formMessageType)
        {
            Message = message;
            Short = shortMessage;
            FormMessageType = formMessageType;
        }
    }
}
