﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoMsg_Module.Models
{
    public class WebAppItem
    {
        public string IdEntryView { get; set; }
        public string NameEntryView { get; set; }
        public string EntryUrl { get; set; }
        public string EntrySessionPath { get; set; }
    }
}
