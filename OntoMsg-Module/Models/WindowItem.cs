﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoMsg_Module.Models
{
    public class WindowItem
    {
        public int X { get; set; }
        public int Y { get; set; }
        public bool ShowCollapseButton { get; set; }
        public int MaxHeight { get; set; }
        public int MaxWidth { get; set; }
        public int MinHeight { get; set; }
        public int MinWidth { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
    }
}
