﻿using OntoMsg_Module.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OntoMsg_Module.Models
{
    public class ViewModelProperty
    {
        public PropertyInfo Property { get; set; }
        public ViewModelAttribute ViewModelAttribute { get; set; }

        public ViewItem ViewItem { get; set; }
    }
}
