﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoMsg_Module.Models
{
    public enum SessionFileType
    {
        Stream = 0,
        User = 1,
        Group = 2
    }
    public class SessionFile
    {
        public StreamWriter StreamWriter { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public Uri FileUri { get; set; }
        public string ExtraUrl { get; set; }
        public bool Exists { get; set; }
        public SessionFileType SessionFileType { get; set; }
    }
}
