﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoMsg_Module
{
    public class ObjectPublisher : IObjectPublisher
    {
        private string guid;

        public event MessageManager.ShowObjectsOfClass _showObjectsOfClass;
        public event MessageManager.ExchangeOItem _exchangeOItem;

        public string IdPublisher
        {
            get;
            private set;
        }

        private SubscriberType subscriberTypes;
        public SubscriberType SubscriberType
        {
            get
            {
                return subscriberTypes;
            }
            set
            {
                subscriberTypes = value;
            }
        }

        private string idPublisherType;
        public string IdPublisherType
        {
            get
            {
                return idPublisherType;
            }
            set
            {
                idPublisherType = value;
            }
        }

        public bool ShowObjectOfClasses(clsOntologyItem classItem)
        {
            if (_showObjectsOfClass != null)
            {
                _showObjectsOfClass(classItem);
                return true;
            }

            return false;
        }

        public bool ExchangeOItem(clsOntologyItem oItem)
        {
            if (_exchangeOItem != null)
            {
                _exchangeOItem(oItem);
                return true;
            }
            return false;
        }

        public ObjectPublisher(string idPublisher)
        {
            IdPublisher = string.IsNullOrEmpty(idPublisher) ? Guid.NewGuid().ToString() : idPublisher;
        }
    }
}
