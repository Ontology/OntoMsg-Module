﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoMsg_Module.Attributes
{
    public enum ViewItemClass
    {
        Other = 0,
        Label = 1,
        Input = 2,
        DateTime = 3,
        Button = 4,
        ImageButton = 5,
        Url = 6,
        IFrame = 7,
        DropDownList = 8,
        ToggleButton = 9,
        Grid = 10,
        Container = 11,
        ContextMenu = 12,
        NumericInput = 13,
        VisGraph = 14,
        TextArea = 15,
        DateTimeInput = 16,
        CheckBox = 17,
        ItemIdentification = 18,
        TreeNode = 19,
        ContextMenuEntry = 20,
        Combobox = 21,
        ListBox = 22,
        NavigationBar = 23,
        ToolBar = 24,
        Selection = 25,
        KendoDropDownList = 26,
        DatePicker = 27,
        FileUpload = 28,
        BreadCrumb = 29,
        BreadCrumbItem = 30,
        Splitter = 31,
        Calendar = 32,
        Menu = 33,
        MenuEntry = 34,
        Window = 35,
        TreeList = 36,
        Badge = 37,
        Anchor = 38,
        All = 4096
    }
    public enum ViewItemType
    {
        Other = 0,
        Content = 1,
        Enable = 2,
        Visible = 3,
        Command = 4,
        Checked = 5,
        IconLabel = 6,
        SelectedIndex = 7,
        DataSource = 8,
        ColumnConfig = 9,
        JsonUrl = 10,
        UrlList = 11,
        Caption = 12,
        JsonConfiguration = 13,
        Change = 14,
        PlaceHolder = 15,
        RemoveIndex = 16,
        Tooltip = 17,
        DoubleClick = 18,
        RemoveItem = 19,
        AddOItem = 20,
        Readonly = 21,
        AddRow = 22,
        Sort = 23,
        GridConfig = 24,
        Filter = 25,
        Config = 26,
        Template = 27,
        Ready = 28,
        RelatedView = 29,
        Active = 30,
        Refreshable = 31,
        All = 4096

    }
    public class ViewModelAttribute : Attribute
    {
        public string ViewItemId { get; set; }
        public ViewItemClass ViewItemClass { get; set; }
        public ViewItemType ViewItemType { get; set; }
        public bool DefaultEnable { get; set; } = false;
        public bool DefaultVisible { get; set; } = true;
        public string DefaultContent { get; set; }
        public bool Send { get; set; }

        public bool NoDefaultValueInit { get; set; }

    }
}
