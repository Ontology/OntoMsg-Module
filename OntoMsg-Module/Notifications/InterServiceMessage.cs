﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoMsg_Module.Notifications
{
    [Flags]
    public enum CommunicationStatus
    {
        None = 0,
        NoReceiver = 1,
        MessageDelivered = 2
    }
    public class InterServiceMessage
    {
        public string SessionId { get; set; }
        public string ChannelId { get; set; }
        public string ReceiverId { get; set; }
        public string SenderId { get; set; }
        public string UserId { get; set; }
        public string GroupId { get; set; }
        public string ViewId { get; set; }
        public CommunicationStatus CommunicationStatus { get; set; }
       
        private List<clsOntologyItem> oItems;

        public List<clsOntologyItem> OItems
        {
            get { return oItems; }
            set
            {
                oItems = value;
            }
        }

        public List<object> GenericParameterItems { get; set; }

        public string ParameterIdentification { get; set; }
    }
}
