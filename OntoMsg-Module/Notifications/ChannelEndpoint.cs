﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoMsg_Module.Notifications
{
    public enum EndpointType
    {
        Receiver = 0,
        Sender = 1
    }
    public class ChannelEndpoint
    {
        public string ChannelTypeId { get; set; }
        public string SessionId { get; set; }
        public string EndPointId { get; set; }
        public EndpointType EndpointType { get; set; }
        public string UserId { get; set; }
    }
}
