﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyAppDBConnector;

namespace OntoMsg_Module
{
    public class clsDataWork_BaseData
    {
        public int Port { get; set; }
        public string Server { get; set; }

        private clsLocalConfig _localConfig;

        private OntologyModDBConnector _dbLevel_MessageServer;
        private OntologyModDBConnector _dbLevel_ServerPort;
        private OntologyModDBConnector _dbLevel_ServerAndPort;

        public clsDataWork_BaseData(clsLocalConfig localConfig)
        {
            _localConfig = localConfig;

            Initialize();
        }

        private void Initialize()
        {
            _dbLevel_MessageServer = new OntologyModDBConnector(_localConfig.Globals);
            _dbLevel_ServerPort = new OntologyModDBConnector(_localConfig.Globals);
            _dbLevel_ServerAndPort = new OntologyModDBConnector(_localConfig.Globals);
        }
    }
}
