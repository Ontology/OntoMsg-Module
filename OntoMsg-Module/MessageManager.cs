﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyClasses.BaseClasses;

namespace OntoMsg_Module
{
    [Flags]
    public enum SubscriberType
    {
        TreeView = 0,
        GridView = 1,
        DetailForm = 2,
        All = 4
    }
    public static class MessageManager
    {
        public delegate void ShowObjectsOfClass(clsOntologyItem classItem);
        public delegate void ExchangeOItem(clsOntologyItem oItem);

        public static List<IObjectPublisher> ObjectPublishers = new List<IObjectPublisher>();
        public static List<ObjectSubscriber> ObjectSubscribers = new List<ObjectSubscriber>();

        public static ObjectPublisher RegisterPublisher(string idPublisherType)
        {
            var objectPublisher = new ObjectPublisher(Guid.NewGuid().ToString().Replace("-", ""));
            objectPublisher.IdPublisherType = idPublisherType;

            var result = RegisterPublisher(objectPublisher);

            if (result)
            {
                return objectPublisher;
            }
            else
            {
                return null;
            }
        }
        public static bool RegisterPublisher(ObjectPublisher objectPublisher)
        {
            var exists = ObjectPublishers.FirstOrDefault(publ => publ == objectPublisher);

            if (exists == null)
            {
                ObjectPublishers.Add(objectPublisher);
            }

            var objectSubscribers = ObjectSubscribers.Where(subs => subs.Publisher == objectPublisher).ToList();

            (from typeSubscriber in ObjectSubscribers.Where(subs => subs.IdPublisherType == objectPublisher.IdPublisherType).ToList() 
             join directSubscriber in objectSubscribers on typeSubscriber equals directSubscriber into directSubscribers
             from directSubscriber in directSubscribers.DefaultIfEmpty()
             where directSubscriber == null
             select typeSubscriber).ToList().ForEach(subs =>
            {
                subs.SetPublisher(objectPublisher);
            });

            return true;
        }

        public static bool UnRegisterPublisher(ObjectPublisher objectPublisher)
        {
            ObjectPublishers.RemoveAll(publ => publ == objectPublisher);
            var objectSubscribers = ObjectSubscribers.Where(subs => subs.Publisher == objectPublisher).ToList();

            (from typeSubscriber in ObjectSubscribers.Where(subs => subs.IdPublisherType == objectPublisher.IdPublisherType).ToList()
             join directSubscriber in objectSubscribers on typeSubscriber equals directSubscriber into directSubscribers
             from directSubscriber in directSubscribers.DefaultIfEmpty()
             where directSubscriber == null
             select typeSubscriber).ToList().ForEach(subs =>
            {
                subs.ClearPublisher();
            });

            return true;
        }

        public static ObjectSubscriber RegisterSubscriber(ObjectPublisher publisher)
        {
            var objectSubscriber = new ObjectSubscriber(Guid.NewGuid().ToString().Replace("-", ""), publisher);
            
            var result = RegisterSubscriber(objectSubscriber);

            if (result)
            {
                objectSubscriber.SetPublisher(publisher);
                return objectSubscriber;
            }
            else
            {
                return null;
            }
        }

        public static bool RegisterSubscriber(ObjectSubscriber objectSubscriber)
        {
            var exists = ObjectSubscribers.FirstOrDefault(subs => subs == objectSubscriber);

            if (exists == null)
            {
                ObjectSubscribers.Add(objectSubscriber);
            }

            return true;
        }

        public static bool UnregisterSubscriber(ObjectSubscriber objectSubscriber)
        {
            ObjectSubscribers.RemoveAll(subs => subs == objectSubscriber);

            return true;
        }
    }
}
