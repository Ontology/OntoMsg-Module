﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
using System.IO.Pipes;
using System.Diagnostics;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using System.Security.Principal;
using OntologyClasses.Interfaces;
using System.Runtime.InteropServices;
using System.Xml;
using OntoMsg_Module.WebSocketServices;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.Models;

namespace OntoMsg_Module
{
    public delegate void ServiceClosed(WebsocketServiceAgent serviceAgent);
    public static class ModuleDataExchanger
    {
        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool WaitNamedPipe(string name, int timeout);

        public delegate void ServerApplyResponse(clsApplyItem[] applyItems);
        public static event ServerApplyResponse _serverApplyResponse;
        public delegate void ServerResponse(string result);
        public static event ServerResponse _serverResponse;
        public static event ServiceClosed _serviceClosed;

        private static Mutex mutexQueue;

        private static clsLogStates logStates = new clsLogStates();
        private static NamedPipeServerStream pipeServer;
        private static IAsyncResult asyncResult;
        

        private static object endpointLocker = new object();
        private static object viewLocker = new object();

        private static ManualResetEvent signal = new ManualResetEvent(false);

        public static List<WebsocketServiceAgent> ServiceAgents { get; private set; }


        private static WebAppItem currentWebApp;
        public static WebAppItem CurrentWebApp
        {
            get { return currentWebApp; }
            set
            {
                currentWebApp = value;
            }
        }

        private static string userGroupPath;
        public static string UserGroupPath
        {
            get { return userGroupPath; }
            set
            {
                userGroupPath = value;
            }
        }

        private static List<WebSocketService> webSocketServices = new List<WebSocketService>();
        public static List<WebSocketService> WebSocketServices
        {
            get { return webSocketServices; }
            set
            {
                webSocketServices = value;
            }
        }


        private static List<ChannelEndpoint> receiverEndpoints = new List<ChannelEndpoint>();
        private static List<ChannelEndpoint> senderEndpoints = new List<ChannelEndpoint>();

        private static List<ViewMetaItem> views = new List<ViewMetaItem>();
        

        public static ViewMetaItem GetViewById(string IdView)
        {
            lock(viewLocker)
            {
                var viewItem = views.FirstOrDefault(view => view.IdView == IdView);
                return viewItem;
            }
            
        }

        public static string GetViewUrlById(string idView, string sender=null, string objectId=null, string classId=null, string relationTypeId=null)
        {
            var viewItem = GetViewById(idView);

            if (viewItem != null)
            {
                var result = CurrentWebApp.EntryUrl + (CurrentWebApp.EntryUrl.EndsWith("/") ? "" : "/") + viewItem.NameCommandLineRun;

                if (sender != null)
                {
                    result += "?Sender=" + sender;
                }
                if (objectId != null)
                {
                    result += "&Object=" + objectId;
                }
                if (classId != null)
                {
                    result += "&Class=" + classId;
                }
                if (relationTypeId != null)
                {
                    result += "&RelationType=" + relationTypeId;
                }

                return result;
            }
            else
            {
                return null;
            }
            
        }

        public static List<ViewMetaItem> GetViewsByName(string NameView)
        {
            lock (viewLocker)
            {
                var viewItems = views.Where(view => view.NameView.ToLower() == NameView).ToList();
                return viewItems;
            }
        }

        public static List<ViewMetaItem> GetViews()
        {
            lock (viewLocker)
            {
                return views;
            }
        }


        public static void AddView(ViewMetaItem view)
        {
            lock(viewLocker)
            {
                views.Add(view);
            }
        }

        public static void ClearViews()
        {
            lock(viewLocker)
            {
                views.Clear();
            }
        }

        private static string pipeName;

        public static bool Server(ILocalConfig localConfig)
        {
            var result = "";
            ManualResetEvent signal = new ManualResetEvent(false);
            if (mutexQueue == null)
            {
                mutexQueue = new Mutex(true, localConfig.IdLocalConfig.Replace("-", ""));
            }
            
            pipeName = localConfig.IdLocalConfig.Replace("-", "");

            int timeout = 0;
            string normalizedPath = System.IO.Path.GetFullPath(
                    string.Format(@"\\.\pipe\{0}", pipeName));
            bool exists = WaitNamedPipe(normalizedPath, timeout);
            bool serverRunning = !exists;

            if (!exists)
            {
                try
                {
                    mutexQueue.WaitOne();
                    pipeServer = new NamedPipeServerStream(pipeName, PipeDirection.In, 1, PipeTransmissionMode.Byte, PipeOptions.Asynchronous);
                    var asyncResult = pipeServer.BeginWaitForConnection(_ => signal.Set(), null);
                    signal.WaitOne();
                    if (asyncResult.IsCompleted)
                    {

                        pipeServer.EndWaitForConnection(asyncResult);
                        using (var streamReader = new StreamReader(pipeServer))
                        {
                            try
                            {
                                result = streamReader.ReadToEnd();

                                XmlSerializer mySerializer = new XmlSerializer(typeof(clsApplyItem[]));
                                // To read the file, create a FileStream.

                                var applyItems = (clsApplyItem[])mySerializer.Deserialize(new StringReader(result));

                                if (_serverApplyResponse != null)
                                {
                                    _serverApplyResponse(applyItems);
                                }


                                


                                
                                
                            }
                            catch(Exception ex)
                            {
                                if (_serverResponse != null)
                                {
                                    _serverResponse(result);
                                }
                            }
                            

                            
                        }

                    }
                    
                    Server(localConfig);
                    
                }
                catch (Exception ex)
                {
                    try
                    {
                        mutexQueue.ReleaseMutex();
                    }
                    catch (Exception ex1)
                    {

                    }
                    serverRunning = false;
                }
            }

            return serverRunning;
        }

        public static void Disconnect()
        {
            try
            {
                signal.Set();
            }
            catch (Exception ex)
            {

            }

            try
            {
                pipeServer.Close();
            }
            catch (Exception ex)
            {

            }

            try
            {
                pipeServer.Dispose();
            }
            catch (Exception ex)
            {

            }

            try
            {
                mutexQueue.ReleaseMutex();
                mutexQueue.Close();
            }
            catch (Exception ex)
            {

            }
            
        }

        public static clsOntologyItem Client(string pipeName, object applyList)
        {
            var result = logStates.LogState_Success.Clone();

            try
            {
                int timeout = 0;
                string normalizedPath = System.IO.Path.GetFullPath(
                    string.Format(@"\\.\pipe\{0}", pipeName));
                bool exists = WaitNamedPipe(normalizedPath, timeout);

                if (exists)
                {
                    NamedPipeClientStream pipeClient = new NamedPipeClientStream(".", pipeName,
                    PipeDirection.Out, PipeOptions.None,
                    TokenImpersonationLevel.Impersonation);

                    pipeClient.Connect(300);

                    XmlSerializer mySerializer = new XmlSerializer(applyList.GetType());

                    using (var streamWriter = new StreamWriter(pipeClient))
                    {
                        mySerializer.Serialize(streamWriter, applyList);
                        streamWriter.Flush();
                    }
                    //StreamString ss = new StreamString(pipeClient);

                    //ss.WriteString(commandLine);
                    pipeClient.Close();
                }
                else
                {
                    result = logStates.LogState_Nothing.Clone();
                }


            }
            catch (WaitHandleCannotBeOpenedException ex)
            {
                result = logStates.LogState_Nothing.Clone();
            }
            catch (Exception ex)
            {
                result = logStates.LogState_Nothing.Clone();
            }

            return result;
        }

        public static clsOntologyItem Client(string pipeName, string commandLine)
        {
            var result = logStates.LogState_Success.Clone();

            try
            {
                int timeout = 0;
                string normalizedPath = System.IO.Path.GetFullPath(
                    string.Format(@"\\.\pipe\{0}", pipeName));
                bool exists = WaitNamedPipe(normalizedPath, timeout);

                if (exists)
                {
                    NamedPipeClientStream pipeClient = new NamedPipeClientStream(".", pipeName,
                    PipeDirection.Out, PipeOptions.None,
                    TokenImpersonationLevel.Impersonation);

                    pipeClient.Connect(300);

                    using (var streamWriter = new StreamWriter(pipeClient))
                    {
                        streamWriter.Write(commandLine);
                        streamWriter.Flush();
                    }
                    //StreamString ss = new StreamString(pipeClient);

                    //ss.WriteString(commandLine);
                    pipeClient.Close();
                }
                else
                {
                    result = logStates.LogState_Nothing.Clone();
                }


            }
            catch (WaitHandleCannotBeOpenedException ex)
            {
                result = logStates.LogState_Nothing.Clone();
            }
            catch (Exception ex)
            {
                result = logStates.LogState_Nothing.Clone();
            }

            return result;

        }

        // Defines the data protocol for reading and writing strings on our stream 
        public class StreamString
        {
            private Stream ioStream;
            private UnicodeEncoding streamEncoding;

            public StreamString(Stream ioStream)
            {
                this.ioStream = ioStream;
                streamEncoding = new UnicodeEncoding();
            }

            public string ReadString()
            {
                int len = 0;

                len = ioStream.ReadByte() * 256;
                len += ioStream.ReadByte();
                byte[] inBuffer = new byte[len];
                ioStream.Read(inBuffer, 0, len);

                return streamEncoding.GetString(inBuffer);
            }

            public int WriteString(string outString)
            {
                byte[] outBuffer = streamEncoding.GetBytes(outString);
                int len = outBuffer.Length;
                if (len > UInt16.MaxValue)
                {
                    len = (int)UInt16.MaxValue;
                }
                ioStream.WriteByte((byte)(len / 256));
                ioStream.WriteByte((byte)(len & 255));
                ioStream.Write(outBuffer, 0, len);
                ioStream.Flush();

                return outBuffer.Length + 2;
            }
        }

        public static void RegisterEndpoint(ChannelEndpoint endpoint)
        {
            lock (endpointLocker)
            {
                if (endpoint.EndpointType == EndpointType.Receiver)
                {
                    if (!receiverEndpoints.Any(rec => rec.EndPointId == endpoint.EndPointId && rec.EndpointType == EndpointType.Receiver && rec.ChannelTypeId == endpoint.ChannelTypeId))
                    {
                        receiverEndpoints.Add(endpoint);
                    }
                }
                else
                {
                    if (!senderEndpoints.Any(send => send.EndPointId == endpoint.EndPointId && send.EndpointType == EndpointType.Sender && send.ChannelTypeId == endpoint.ChannelTypeId))
                    {
                        senderEndpoints.Add(endpoint);
                    }
                }

            }
        }

        public static void UnregisterEndpoints(string endpointId)
        {
            lock (endpointLocker)
            {
                senderEndpoints.RemoveAll(endp => endp.EndPointId == endpointId);
                receiverEndpoints.RemoveAll(endp => endp.EndPointId == endpointId);
            }
        }

        public static List<ChannelEndpoint> GetReceivers(string channelTypeId, string sessionId, string userId = null)
        {
            if (userId == null)
            {
                return receiverEndpoints.Where(rec => rec.ChannelTypeId == channelTypeId && rec.SessionId == sessionId && rec.EndpointType == EndpointType.Receiver).ToList();
            }
            else
            {
                return receiverEndpoints.Where(rec => rec.ChannelTypeId == channelTypeId && rec.UserId == userId && rec.EndpointType == EndpointType.Receiver).ToList();
            }
            
        }

        public static bool RegisterServiceAgent(WebsocketServiceAgent serviceAgent)
        {
            if (ServiceAgents == null)
            {
                ServiceAgents = new List<WebsocketServiceAgent>();
            }

            serviceAgent.closedSocket += ServiceAgent_channelClosed;
            ServiceAgents.Add(serviceAgent);

            return true;
        }

        private static void ServiceAgent_channelClosed(WebsocketServiceAgent serviceAgent)
        {
            if (_serviceClosed != null)
            {
                _serviceClosed(serviceAgent);
            }
            ServiceAgents.Remove(serviceAgent);
            serviceAgent.Dispose();
            serviceAgent = null;
            GC.Collect();
        }



    }
}
