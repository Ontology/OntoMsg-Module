﻿using OntologyAppDBConnector.Base;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OntoMsg_Module.Base
{
    public abstract class ViewModelBase : NotifyPropertyChange, IViewModel
    {
        public List<ViewModelProperty> ViewModelProperties { get; set; }
        public List<ViewModelProperty> ViewModelSendProperties { get; set; }

        public ViewModelBase()
        {
            var properties = this.GetType().GetProperties().Cast<PropertyInfo>().ToList();
            ViewModelProperties = (properties.Select(prop =>
            {
                var viewModelAttribute = (ViewModelAttribute)prop.GetCustomAttribute(typeof(ViewModelAttribute));
                if (viewModelAttribute == null) return null;

                var viewItemId = viewModelAttribute.ViewItemId;
                if (viewItemId == null) viewItemId = prop.Name;
                var viewItemType = viewModelAttribute.ViewItemType;
                var viewItemClass = viewModelAttribute.ViewItemClass;



                return new ViewModelProperty { Property = prop,
                    ViewModelAttribute = viewModelAttribute,
                    ViewItem = new ViewItem
                    {
                        ViewItemId = viewItemId,
                        ViewItemClass = viewItemClass.ToString(),
                        ViewItemType = viewItemType.ToString()

                    }
                };
            }).Where(viewItemProp => viewItemProp != null).ToList());

            ViewModelSendProperties = ViewModelProperties.Where(viewModelProperty => viewModelProperty.ViewModelAttribute != null && viewModelProperty.ViewModelAttribute.Send).ToList();
        }
    }
}
