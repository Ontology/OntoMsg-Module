﻿using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoMsg_Module.WebSocketServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoMsg_Module.Base
{
    public interface IViewController
    {
        void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent);

        List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All);
    }
}
