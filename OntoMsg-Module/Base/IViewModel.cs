﻿using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoMsg_Module.Base
{
    public interface IViewModel
    {
        List<ViewModelProperty> ViewModelProperties { get; set; }
        List<ViewModelProperty> ViewModelSendProperties { get; set; }
    }
}
