﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using WebSocketSharp;
using WebSocketSharp.Server;

namespace OntoMsg_Module.WebSocketServices
{
    public delegate void ChannelOpened();
    public delegate void ChannelClosing();
    public delegate void ChannelClosed(WebsocketServiceAgent serviceAgent);
    public delegate void ErrorOccured(ErrorEventArgs e);
    public delegate void ComServerOpened();
    public delegate void ComServerOnMessage(InterServiceMessage message);
    public delegate void LoginResultReceived(bool success);
    public delegate void ReceivedData(byte[] data);
    public delegate void ParamsChecked();
    

    public class WebsocketServiceAgent : WebSocketBehavior, IDisposable
    {
        /// <summary>
        /// If an event is changed, the event will be fired for layered communication
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        public event ComServerOpened comServerOpened;
        public event ComServerOnMessage comServerOnMessage;
        public event LoginResultReceived loginResultReceived;
        public event ChannelClosed channelClosed;
        public event ReceivedData receivedData;
        public event ParamsChecked paramsChecked;
        public event ChannelClosed closedSocket;

        private bool isUserReceiver;

        public string EndpointId { get; set; }
        public string WebSocketPath { get; private set; }
        public clsOntologyItem oItemUser { get; set; }
        public clsOntologyItem oItemGroup { get; set; }

        public bool InterpretNextAsBytes { get; set; }
        private long byteRequestCount;
        private long byteRequestIx;

        public bool IsLoginController { get; set; }

        private FileSystemServiceAgent fileSystemServiceAgent;

        public SessionFile RequestWriteStream(string fileName)
        {
            
            fileSystemServiceAgent = GetFileSystemObject();
            
            return fileSystemServiceAgent.RequestWriteStream(DataText_SessionId, fileName);
        }

        public SessionFile RequestUserFile(string idUser, string fileName)
        {

            fileSystemServiceAgent = GetFileSystemObject();

            return fileSystemServiceAgent.RequestUserFile(idUser, fileName, false);
        }

        public void RemoveAllResources()
        {
            fileSystemServiceAgent?.RemoveAllResources();
        }

        public void RemoveResource(string fileName)
        {
            fileSystemServiceAgent?.RemoveResource(fileName);
        }

        private Globals globals;

        private JsonSerializerSettings jsonConverterSettings = new JsonSerializerSettings();
        

        protected virtual void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Event, if websocket is opened
        /// </summary>
        public event ChannelOpened channelOpened;

        /// <summary>
        /// Event, if websocket is closing
        /// </summary>
        public event ChannelClosing channelClosing;

        private List<ViewParameter> viewArguments = new List<ViewParameter>();
        public List<ViewParameter> ViewArguments
        {
            get { return viewArguments; }
            set
            {
                viewArguments = value;

                var dedicatedSender = viewArguments.FirstOrDefault(arg => arg.StandardParameter == StandardParameter.Sender);

                if (dedicatedSender != null)
                {
                    DedicatedSenderArgument = dedicatedSender;
                }

                var attributeType = viewArguments.FirstOrDefault(arg => arg.StandardParameter == StandardParameter.AttributeType);

                if (attributeType != null)
                {
                    AttributeTypeArgument = attributeType;
                }

                var relationType = viewArguments.FirstOrDefault(arg => arg.StandardParameter == StandardParameter.RelationType);

                if (relationType != null)
                {
                    RelationTypeArgument = relationType;
                }

                var classItem = viewArguments.FirstOrDefault(arg => arg.StandardParameter == StandardParameter.Class);

                if (classItem != null)
                {
                    ClassArgument = classItem;
                }

                var objectItem = viewArguments.FirstOrDefault(arg => arg.StandardParameter == StandardParameter.Object);

                if (objectItem != null)
                {
                    ObjectArgument = objectItem;
                }

                var viewItem = viewArguments.FirstOrDefault(arg => arg.StandardParameter == StandardParameter.View);

                if (viewItem != null)
                {
                    ViewArgument = viewItem;
                }
                RaisePropertyChanged(NotifyChanges.Websocket_ViewArguments);
            }
        }

        private ViewParameter dedicatedSenderArgument;
        public ViewParameter DedicatedSenderArgument
        {
            get
            {
                return dedicatedSenderArgument;
            }
            private set
            {
                dedicatedSenderArgument = value;
                RaisePropertyChanged(NotifyChanges.Websocket_DedicatedSenderArgument);
            }
        }

        private ViewParameter attributeTypeArgument;
        public ViewParameter AttributeTypeArgument
        {
            get
            {
                return attributeTypeArgument;
            }
            private set
            {
                attributeTypeArgument = value;
                RaisePropertyChanged(NotifyChanges.Websocket_AttributeTypeArgument);
            }
        }

        private ViewParameter relationTypeArgument;
        public ViewParameter RelationTypeArgument
        {
            get
            {
                return relationTypeArgument;
            }
            private set
            {
                relationTypeArgument = value;
                RaisePropertyChanged(NotifyChanges.Websocket_RelationTypeArgument);
            }
        }

        private ViewParameter objectArgument;
        public ViewParameter ObjectArgument
        {
            get
            {
                return objectArgument;
            }
            private set
            {
                objectArgument = value;
                RaisePropertyChanged(NotifyChanges.Websocket_ObjectArgument);
            }
        }

        private ViewParameter classArgument;
        public ViewParameter ClassArgument
        {
            get
            {
                return classArgument;
            }
            private set
            {
                classArgument = value;
                RaisePropertyChanged(NotifyChanges.Websocket_ClassArgument);
            }
        }

        private ViewParameter viewArgument;
        public ViewParameter ViewArgument
        {
            get
            {
                return viewArgument;
            }
            private set
            {
                viewArgument = value;
                RaisePropertyChanged(NotifyChanges.Websocket_ViewArgument);
            }
        }


        public event ErrorOccured errorOccured;

        private WebSocket webSocketComServerClient;

        private IViewController viewController;

        /// <summary>
        /// For messages which must be sent to the connected client
        /// </summary>
        private string dataText_SessionId;
        public string DataText_SessionId
        {
            get { return dataText_SessionId; }
            set
            {
                dataText_SessionId = value;
                RaisePropertyChanged(NotifyChanges.Websocket_DataText_SessionId);
            }
        }

        private List<ViewItem> changedViewItems;
        public List<ViewItem> ChangedViewItems
        {
            get { return changedViewItems; }
            set
            {
                changedViewItems = value;
                RaisePropertyChanged(NotifyChanges.Websocket_ChangedViewItems);
            }
        }

        private IWebSocketSession socketSessionItem;

        /// <summary>
        /// Logstates for methods
        /// </summary>
        private clsLogStates logStates = new clsLogStates();

        /// <summary>
        /// Request-Dictionary (Json-Source) from client
        /// </summary>
        public Dictionary<string, object> Request { get; set; }


        private string command_RequestedCommand;
        /// <summary> 
        /// A requested command from client
        /// </summary>
        public string Command_RequestedCommand
        {
            get { return command_RequestedCommand; }
            set
            {
                command_RequestedCommand = value;
                RaisePropertyChanged(NotifyChanges.Websocket_Command_RequestedCommand);
            }
        }

        private KeyValuePair<string, object> changedProperty;
        public KeyValuePair<string, object> ChangedProperty
        {
            get { return changedProperty; }
            set
            {
                changedProperty = value;
                RaisePropertyChanged(NotifyChanges.Websocket_ChangedProperty);
            }
        }

        private string command_RequestEvent;
        /// <summary> 
        /// A requested command from client
        /// </summary>
        public string Command_RequestEvent
        {
            get { return command_RequestEvent; }
            set
            {
                command_RequestEvent = value;
                RaisePropertyChanged(NotifyChanges.Websocket_Event_RequestEvent);
            }
        }

        public string IdEntryView { get; private set; }

        /// <summary>
        /// If websocket is closing
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClose(CloseEventArgs e)
        {
            UnregisterEndpoints();
            
            if (channelClosing != null)
            {
                channelClosing();
            }
            viewController = null;
            if (channelClosed != null)
            {
                channelClosed(this);
            }
        }

        /// <summary>
        /// Message from the client
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMessage(MessageEventArgs e)
        {
            if (InterpretNextAsBytes)
            {
                byteRequestIx++;
                if (receivedData != null)
                {
                    receivedData(e.RawData);
                }
                
                if (byteRequestIx == byteRequestCount)
                {
                    InterpretNextAsBytes = false;
                }
            }
            else
            {
                try
                {
                    Request = JsonConvert.DeserializeObject<Dictionary<string, object>>(e.Data);
                    if (Request.ContainsKey(OntoMsg_Module.WebSocketServices.WebSocketBase.Property_MessageType) && Request[OntoMsg_Module.WebSocketServices.WebSocketBase.Property_MessageType].ToString() == OntoMsg_Module.WebSocketServices.WebSocketBase.MessageType_Command)
                    {
                        Command_RequestedCommand = Request[OntoMsg_Module.WebSocketServices.WebSocketBase.MessageType_Command].ToString();
                        if (Command_RequestedCommand == "UploadFile" && Request.ContainsKey("Switch") && Request.ContainsKey("Count"))
                        {
                            InterpretNextAsBytes = (bool)Request["Switch"];
                            byteRequestCount = (long)Request["Count"];
                            byteRequestIx = 0;
                        }
                        
                    }
                    else if (Request.ContainsKey(OntoMsg_Module.WebSocketServices.WebSocketBase.Property_MessageType) && Request[OntoMsg_Module.WebSocketServices.WebSocketBase.Property_MessageType].ToString() == OntoMsg_Module.WebSocketServices.WebSocketBase.MessageType_Event)
                    {
                        Command_RequestEvent = Request[OntoMsg_Module.WebSocketServices.WebSocketBase.MessageType_Event].ToString();
                    }
                    else if (Request.ContainsKey(OntoMsg_Module.WebSocketServices.WebSocketBase.Property_MessageType) && Request[OntoMsg_Module.WebSocketServices.WebSocketBase.Property_MessageType].ToString() == OntoMsg_Module.WebSocketServices.WebSocketBase.MessageType_Change)
                    {
                        if (Request.ContainsKey(OntoMsg_Module.WebSocketServices.WebSocketBase.Property_PropertyName))
                        {
                            var propertyName = Request[OntoMsg_Module.WebSocketServices.WebSocketBase.Property_PropertyName].ToString();
                            if (!Request.ContainsKey(propertyName)) return;

                            ChangedProperty = new KeyValuePair<string, object>(propertyName, Request[propertyName]);
                        }
                    }
                    else if (Request.ContainsKey(WebSocketBase.Property_MessageType) && Request[WebSocketBase.Property_MessageType].ToString() == "ViewItem")
                    {

                        ChangedViewItems = JsonConvert.DeserializeObject<List<ViewItem>>(Request["Items"].ToString());
                        ChangedViewItems.ForEach(changeItem =>
                        {
                            if (changeItem.ViewItemId == "parameters" &&
                                        changeItem.ViewItemClass == ViewItemClass.Other.ToString() &&
                                        changeItem.ViewItemType == ViewItemType.Other.ToString())
                            {
                                ViewArguments = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(changeItem.ViewItemValue.ToString()).Select(itm => new ViewParameter(itm)).ToList();
                                paramsChecked?.Invoke();
                                return;
                            }

                            var propertyItem = viewController.GetViewModelProperties().FirstOrDefault(viewModelProperty => 
                                viewModelProperty.ViewModelAttribute.ViewItemId == changeItem.ViewItemId && 
                                viewModelProperty.ViewModelAttribute.ViewItemType.ToString() == changeItem.ViewItemType &&
                                viewModelProperty.ViewModelAttribute.ViewItemClass.ToString() == changeItem.ViewItemClass);

                            if (propertyItem != null)
                            {
                                if (changeItem.ViewItemValue == null)
                                {
                                    propertyItem.Property.SetValue(viewController, null);
                                }
                                else
                                {
                                    
                                    if (propertyItem.Property.PropertyType == typeof(bool) ||
                                        propertyItem.Property.PropertyType == typeof(bool?))
                                    {

                                        if (changeItem.ViewItemValue.GetType() == typeof(string))
                                        {
                                            bool checkValue;
                                            if (changeItem.ViewItemValue.ToString().ToLower() == "on")
                                            {
                                                propertyItem.Property.SetValue(viewController, true);
                                            }
                                            else if (changeItem.ViewItemValue.ToString().ToLower() == "off")
                                            {
                                                propertyItem.Property.SetValue(viewController, false);
                                            }
                                            else if (bool.TryParse(changeItem.ViewItemValue.ToString(), out checkValue))
                                            {
                                                propertyItem.Property.SetValue(viewController, checkValue);
                                            }
                                            
                                        }
                                        else
                                        {
                                            propertyItem.Property.SetValue(viewController, changeItem.ViewItemValue);
                                        }


                                    }
                                    else if (propertyItem.Property.PropertyType == typeof(DateTime) ||
                                             propertyItem.Property.PropertyType == typeof(DateTime?))
                                    {
                                        if (changeItem.ViewItemValue.GetType() == typeof(string))
                                        {
                                            DateTime checkValue;
                                            if (DateTime.TryParse(changeItem.ViewItemValue.ToString(), out checkValue))
                                            {
                                                propertyItem.Property.SetValue(viewController, checkValue);
                                            }
                                        }
                                        else
                                        {
                                            DateTime checkValue = (DateTime)changeItem.ViewItemValue;
                                            checkValue = checkValue.ToLocalTime();
                                            propertyItem.Property.SetValue(viewController, checkValue);
                                        }


                                    }
                                    else if (propertyItem.Property.PropertyType == typeof(long) ||
                                             propertyItem.Property.PropertyType == typeof(long?))
                                    {
                                        if (changeItem.ViewItemValue.GetType() == typeof(string))
                                        {
                                            long checkValue;
                                            if (long.TryParse(changeItem.ViewItemValue.ToString(), out checkValue))
                                            {
                                                propertyItem.Property.SetValue(viewController, checkValue);
                                            }
                                        }
                                        else
                                        {
                                            propertyItem.Property.SetValue(viewController, changeItem.ViewItemValue);
                                        }

                                    }
                                    else if (propertyItem.Property.PropertyType == typeof(int) ||
                                             propertyItem.Property.PropertyType == typeof(int?))
                                    {
                                        if (changeItem.ViewItemValue.GetType() == typeof(string))
                                        {
                                            int checkValue;
                                            if (int.TryParse(changeItem.ViewItemValue.ToString(), out checkValue))
                                            {
                                                propertyItem.Property.SetValue(viewController, checkValue);
                                            }
                                        }
                                        else
                                        {
                                            propertyItem.Property.SetValue(viewController, changeItem.ViewItemValue);
                                        }


                                    }
                                    else if (propertyItem.Property.PropertyType == typeof(double) ||
                                             propertyItem.Property.PropertyType == typeof(double?))
                                    {
                                        if (changeItem.ViewItemValue.GetType() == typeof(string))
                                        {
                                            double checkValue;
                                            if (double.TryParse(changeItem.ViewItemValue.ToString(), out checkValue))
                                            {
                                                propertyItem.Property.SetValue(viewController, checkValue);
                                            }
                                        }
                                        else
                                        {
                                            propertyItem.Property.SetValue(viewController, changeItem.ViewItemValue);
                                        }


                                    }
                                    else if (propertyItem.Property.PropertyType == typeof(string))
                                    {
                                        propertyItem.Property.SetValue(viewController, changeItem.ViewItemValue.ToString());
                                    }

                                }
                            }
                        });
                    }
                }
                catch (Exception ex)
                {

                }
            }
            
            return;

        }

        public void CloseSocket(string logMessage)
        {
            Context.WebSocket.Log.Fatal(logMessage);
            Context.WebSocket.Close();
        }

        /// <summary>
        /// If websocket is opening
        /// </summary>
        protected override void OnOpen()
        {
            if (!Context.QueryString.Contains(OntoMsg_Module.WebSocketServices.WebSocketBase.QueryParam_Module))
            {
                Context.WebSocket.Log.Fatal("No Module-Param!");
                Context.WebSocket.Close();
                return;
            }
            
            if (!Context.QueryString.Contains(OntoMsg_Module.WebSocketServices.WebSocketBase.QueryParam_Controller))
            {
                
                Context.WebSocket.Log.Fatal("No Controller-Param!");
                Context.WebSocket.Close();
                return;
                
            }
            if (!Context.QueryString.Contains(OntoMsg_Module.WebSocketServices.WebSocketBase.QueryParam_Session))
            {
                Context.WebSocket.Log.Fatal("No Session-Param!");
                Context.WebSocket.Close();
                return;
            }

            if (Context.QueryString.Contains(OntoMsg_Module.WebSocketServices.WebSocketBase.QueryParam_View))
            {
                IdEntryView = Context.QueryString[OntoMsg_Module.WebSocketServices.WebSocketBase.QueryParam_View];
            }

            var moduleKey = Context.QueryString[OntoMsg_Module.WebSocketServices.WebSocketBase.QueryParam_Module];
            var controllerKey = Context.QueryString[OntoMsg_Module.WebSocketServices.WebSocketBase.QueryParam_Controller];
            var webSocketServiceItem = ModuleDataExchanger.WebSocketServices.FirstOrDefault(serviceItem => serviceItem.Key == moduleKey);
            if (webSocketServiceItem == null)
            {
                Context.WebSocket.Log.Fatal("No Module found in Module-List!");
                Context.WebSocket.Close();
                return;
            }
            WebSocketPath = webSocketServiceItem.WebSocketPath;
            var webSocketControllerItem = webSocketServiceItem.WebSocketControllers.FirstOrDefault(controllerItem => controllerItem.Key == controllerKey);

            if (webSocketControllerItem == null)
            {
                Context.WebSocket.Log.Fatal("No Controller found in Module-List!");
                Context.WebSocket.Close();
                return;
            }


            var assemblyPath = webSocketServiceItem.AssemblyPath;
            var assemblyName = webSocketServiceItem.Name;
            var serviceAssembly = AppDomain.CurrentDomain.Load(assemblyName);

            var initializeClass = serviceAssembly.ExportedTypes.FirstOrDefault(typeItem => typeItem.Name == webSocketControllerItem.ClassName);

          
            
            
            if (initializeClass != null)
            {
                var controllerManager = (IViewController)serviceAssembly.CreateInstance(initializeClass.FullName);
                controllerManager.InitializeViewController(this);
                this.viewController = controllerManager;
                var dataText_SessionId = Context.QueryString[OntoMsg_Module.WebSocketServices.WebSocketBase.QueryParam_Session];
                if (string.IsNullOrEmpty(dataText_SessionId))
                {
                    dataText_SessionId = Guid.NewGuid().ToString();
                }
                DataText_SessionId = dataText_SessionId;

                var dataText_endPointId = Context.QueryString[OntoMsg_Module.WebSocketServices.WebSocketBase.QueryParam_Endpoint];
                if (string.IsNullOrEmpty(dataText_endPointId))
                {
                    dataText_endPointId = Guid.NewGuid().ToString();
                }
                EndpointId = dataText_endPointId;
            }
            else
            {
                
                Context.WebSocket.Log.Fatal("No Controller found!");
                Context.WebSocket.Close();
                return;
            }
            if (channelOpened != null)
            {
                channelOpened();
            }
        }

        protected override void OnError(ErrorEventArgs e)
        {
            base.OnError(e);
            if (errorOccured == null) return;
            errorOccured(e);
        }


        /// <summary>
        /// Command for sending the model of the view-controller
        /// </summary>
        /// <returns></returns>
        public clsOntologyItem SendModel(bool initialize = false)
        {
            
            if (viewController == null) return logStates.LogState_Error.Clone();

            if (socketSessionItem == null)
            {
                socketSessionItem = Sessions.Sessions.FirstOrDefault(session => session.Context.QueryString[WebSocketBase.QueryParam_Session] == DataText_SessionId && session.Context.QueryString[WebSocketBase.QueryParam_Endpoint] == EndpointId);
            }

            if (socketSessionItem == null) return logStates.LogState_Error.Clone();

            var properties = viewController.GetViewModelProperties(onlySend: true);
            if (initialize)
            {
                properties.ForEach(prop =>
                {

                    prop.ViewItem.AddValue(prop.Property.GetValue(viewController));
                });
            }

            var sendItems = properties.Where(prop => initialize ? 1 == 1 : prop.ViewItem.HasChanged).Select(prop => prop.ViewItem).ToList();

            try
            {
                
                var jsonSend = JsonConvert.SerializeObject(sendItems, jsonConverterSettings);
                Sessions.SendTo(jsonSend, socketSessionItem.ID);
                sendItems.ForEach(sendItem =>
                {
                    sendItem.ClearValues();
                });
            }
            catch (Exception ex)
            {
                var result = logStates.LogState_Success.Clone();
                result.Additional1 = ex.Message;
                return result;
            }


            return logStates.LogState_Success.Clone();
        }

        /// <summary>
        /// Send a json to the client
        /// </summary>
        /// <param name="dictToSend"></param>
        /// <returns></returns>
        public clsOntologyItem SendDictionary(Dictionary<string, object> dictToSend)
        {
            try
            {
                if (socketSessionItem == null)
                {
                    socketSessionItem = Sessions.Sessions.FirstOrDefault(session => session.Context.QueryString[WebSocketBase.QueryParam_Session] == DataText_SessionId && session.Context.QueryString[WebSocketBase.QueryParam_Endpoint] == EndpointId);
                }

                if (socketSessionItem == null) return logStates.LogState_Error.Clone();

                var jsonSend = JsonConvert.SerializeObject(dictToSend, Newtonsoft.Json.Formatting.Indented);

                Sessions.SendTo(jsonSend, socketSessionItem.ID);
            }
            catch (Exception ex)
            {
                var result = logStates.LogState_Success.Clone();
                result.Additional1 = ex.Message;
                return result;
            }

            return logStates.LogState_Success.Clone();
        }

        public clsOntologyItem SendPropertyChange(string propertyName, object viewModel = null)
        {
            try
            {
                
                if (viewModel != null && viewModel is ViewModelBase)
                {
                    ViewModelBase viewModelToSend = (ViewModelBase)viewModel;
                    var property = viewModelToSend.ViewModelProperties.FirstOrDefault(prop => prop.Property.Name == propertyName);
                    if (property == null) return logStates.LogState_Error.Clone();
                    if (!property.ViewItem.HasChanged) return logStates.LogState_Success.Clone();
                    if (socketSessionItem == null)
                    {
                        socketSessionItem = Sessions.Sessions.FirstOrDefault(session => session.Context.QueryString[WebSocketBase.QueryParam_Session] == DataText_SessionId && session.Context.QueryString[WebSocketBase.QueryParam_Endpoint] == EndpointId);
                    }

                    if (socketSessionItem == null) return logStates.LogState_Error.Clone();

                    var sendItems = new List<ViewItem> { property.ViewItem };

                    var jsonSend = JsonConvert.SerializeObject(sendItems, jsonConverterSettings);

                    property.ViewItem.ClearValues();
                    Sessions.SendTo(jsonSend, socketSessionItem.ID);

                }
                else
                {
                    var property = viewController.GetViewModelProperties().FirstOrDefault(prop => prop.Property.Name == propertyName);
                    if (property == null) return logStates.LogState_Error.Clone();
                    if (!property.ViewItem.HasChanged) return logStates.LogState_Success.Clone();
                    if (socketSessionItem == null)
                    {
                        socketSessionItem = Sessions.Sessions.FirstOrDefault(session => session.Context.QueryString[WebSocketBase.QueryParam_Session] == DataText_SessionId && session.Context.QueryString[WebSocketBase.QueryParam_Endpoint] == EndpointId);
                    }

                    if (socketSessionItem == null) return logStates.LogState_Error.Clone();

                    var sendItems = new List<ViewItem> { property.ViewItem };

                    var jsonSend = JsonConvert.SerializeObject(sendItems, jsonConverterSettings);

                    property.ViewItem.ClearValues();
                    Sessions.SendTo(jsonSend, socketSessionItem.ID);

                }
                


            }
            catch (Exception ex)
            {
                var result = logStates.LogState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;
            }

            return logStates.LogState_Success.Clone();
        }

        public clsOntologyItem SendViewItems(List<ViewItem> viewItems)
        {
            try
            {
                var jsonSend = JsonConvert.SerializeObject(viewItems, jsonConverterSettings);

                Sessions.SendTo(jsonSend, socketSessionItem.ID);
            }
            catch(Exception ex)
            {
                var result = logStates.LogState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;
            }

            return logStates.LogState_Success.Clone();
        }

        public clsOntologyItem SendBytes(byte[] bytes)
        {
            try
            {
                if (socketSessionItem == null)
                {
                    socketSessionItem = Sessions.Sessions.FirstOrDefault(session => session.Context.QueryString[WebSocketBase.QueryParam_Session] == DataText_SessionId && session.Context.QueryString[WebSocketBase.QueryParam_Endpoint] == EndpointId);
                }

                if (socketSessionItem == null) return logStates.LogState_Error.Clone();

                Sessions.SendTo(bytes, socketSessionItem.ID);
            }
            catch (Exception ex)
            {
                var result = logStates.LogState_Success.Clone();
                result.Additional1 = ex.Message;
                return result;
            }

            return logStates.LogState_Success.Clone();
        }

        public clsOntologyItem SendString(string content)
        {
            try
            {
                if (socketSessionItem == null)
                {
                    socketSessionItem = Sessions.Sessions.FirstOrDefault(session => session.Context.QueryString[WebSocketBase.QueryParam_Session] == DataText_SessionId && session.Context.QueryString[WebSocketBase.QueryParam_Endpoint] == EndpointId);
                }

                if (socketSessionItem == null) return logStates.LogState_Error.Clone();

                Sessions.SendTo(content, socketSessionItem.ID);
            }
            catch (Exception ex)
            {
                var result = logStates.LogState_Success.Clone();
                result.Additional1 = ex.Message;
                return result;
            }

            return logStates.LogState_Success.Clone();
        }

        public clsOntologyItem SendCommand(string command, Dictionary<string, object> parameters = null)
        {
            try
            {
                if (socketSessionItem == null)
                {
                    socketSessionItem = Sessions.Sessions.FirstOrDefault(session => session.Context.QueryString[WebSocketBase.QueryParam_Session] == DataText_SessionId && session.Context.QueryString[WebSocketBase.QueryParam_Endpoint] == EndpointId);
                }

                if (socketSessionItem == null) return logStates.LogState_Error.Clone();
                var viewItem = new ViewItem
                {
                    ViewItemId = command,
                    ViewItemType = ViewItemType.Command.ToString()
                };
                var sendItems = new List<ViewItem> { viewItem };
                
                if (parameters != null)
                {
                    var dict = new Dictionary<string, object>();
                    foreach (var param in parameters)
                    {
                        dict.Add(param.Key, param.Value);
                    }
                    viewItem.AddValue(dict);
                }
                
                var jsonSend = JsonConvert.SerializeObject(sendItems, Newtonsoft.Json.Formatting.Indented);
                Sessions.SendTo(jsonSend, socketSessionItem.ID);
            }
            catch (Exception ex)
            {
                var result = logStates.LogState_Success.Clone();
                result.Additional1 = ex.Message;
                return result;
            }

            return logStates.LogState_Success.Clone();
        }

        public void RegisterEndpoint(ChannelEndpoint endpoint)
        {
            
            endpoint.SessionId = DataText_SessionId;
            endpoint.EndPointId = EndpointId;
            endpoint.UserId = oItemUser != null ? oItemUser.GUID : null;

            ModuleDataExchanger.RegisterEndpoint(endpoint);

            if (webSocketComServerClient != null) return;
            webSocketComServerClient = new WebSocket("wss://" + WebSocketBase.ComServerIP + ":" + WebSocketBase.ComServerPort.ToString() + "/ModuleCommunicator?EndpointId=" + endpoint.EndPointId + "&Session=" + DataText_SessionId);
            webSocketComServerClient.OnOpen += WebSocketClient_OnOpen;
            webSocketComServerClient.OnMessage += WebSocketComServerClient_OnMessage;
            webSocketComServerClient.Connect();
        }

        public void UnregisterEndpoints()
        {
            ModuleDataExchanger.UnregisterEndpoints(EndpointId);
        }

        private void WebSocketComServerClient_OnMessage(object sender, MessageEventArgs e)
        {
            
            try
            {
                if (e.Data != null)
                {
                    var message = Newtonsoft.Json.JsonConvert.DeserializeObject<InterServiceMessage>(e.Data.ToString());
                    if (message != null)
                    {
                        if (message.ChannelId != Channels.Login && DedicatedSenderArgument != null 
                            && !string.IsNullOrEmpty(message.SenderId) 
                            && DedicatedSenderArgument.Value != message.SenderId) return;
                        if (message.ChannelId == Channels.Login && !IsLoginController)
                        {
                            if (loginResultReceived == null)
                            {
                                CloseSocket("Authentication-Handler is missing");
                            }
                            if (message.OItems == null || message.OItems.Count != 2)
                            {
                                loginResultReceived(false);
                                
                                return;
                            }

                            if (message.OItems.First().GUID == logStates.LogState_Error.GUID)
                            {
                                
                                
                                loginResultReceived(false);
                                
                                return;
                            }

                            if (message.CommunicationStatus == CommunicationStatus.NoReceiver)
                            {
                                
                                loginResultReceived(false);
                                
                                return;
                            }

                            oItemUser = message.OItems[0];
                            oItemGroup = message.OItems[1];

                            if (oItemUser == null || oItemGroup == null)
                            {
                                
                                loginResultReceived(false);
                                
                                return;
                            }

                            isUserReceiver = oItemUser.Mark.Value;
                            
                            GetFileSystemObject();

                            loginResultReceived(true);
                        }
                        if (comServerOnMessage != null)
                        {
                            if (message.ReceiverId != null && message.ReceiverId != EndpointId) return;
                            comServerOnMessage(message);
                        }
                    }
                }
            }
            catch (Exception ex) { }

        }

        public FileSystemServiceAgent GetFileSystemObject()
        {
            if (fileSystemServiceAgent == null)
            {
                return new FileSystemServiceAgent(OntoMsg_Module.ModuleDataExchanger.CurrentWebApp.EntryUrl, OntoMsg_Module.ModuleDataExchanger.CurrentWebApp.EntrySessionPath, oItemUser, oItemGroup);
            }
            else
            {
                return fileSystemServiceAgent;
            }
            
        }

        public void SendInterModMessage(InterServiceMessage interServiceMessage)
        {
            interServiceMessage.SenderId = EndpointId;
            interServiceMessage.SessionId = DataText_SessionId;

            if (isUserReceiver)
            {
                interServiceMessage.UserId = oItemUser.GUID;
            }

            if (interServiceMessage.ChannelId != Channels.Login && DedicatedSenderArgument != null)
            {
                interServiceMessage.ReceiverId = DedicatedSenderArgument.Value;
            }
            var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(interServiceMessage);
            webSocketComServerClient.Send(jsonString);
            
            
        }

        public void SetVisibility(bool isVisible)
        {
            var visibilityProperties = viewController.GetViewModelProperties(viewItemType: ViewItemType.Visible);

            visibilityProperties.ForEach(prop => {
                prop.Property.SetValue(viewController, isVisible);
                prop.ViewItem.AddValue(isVisible);
            });
        }

        public void SetEnable(bool isEnabled)
        {
            var enableProperties = viewController.GetViewModelProperties(viewItemType: ViewItemType.Enable);

            enableProperties.ForEach(prop => {
                prop.Property.SetValue(viewController, isEnabled);
                prop.ViewItem.AddValue(isEnabled);
            });
        }

        private void WebSocketClient_OnOpen(object sender, EventArgs e)
        {
            if (comServerOpened != null)
            {
                comServerOpened();
            }            
        }

        public void Dispose()
        {
            
        }

        public WebsocketServiceAgent()
        {
            jsonConverterSettings.NullValueHandling = NullValueHandling.Ignore;
            EndpointId = Guid.NewGuid().ToString();
            ModuleDataExchanger.RegisterServiceAgent(this);
        }
    }

}
