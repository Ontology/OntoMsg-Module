﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSocketSharp.Server;
using WebSocketSharp;
using OntoMsg_Module.Notifications;

namespace OntoMsg_Module.WebSocketServices
{
    public class ComServiceAgent : WebSocketBehavior
    {

        private IWebSocketSession socketSession;

        /// <summary>
        /// If websocket is closing
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClose(CloseEventArgs e)
        {
          
        }

        /// <summary>
        /// Message from the client
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMessage(MessageEventArgs e)
        {
            try
            {
                try
                {
                    IWebSocketSession senderSocketSession;
                    var message = Newtonsoft.Json.JsonConvert.DeserializeObject<InterServiceMessage>(e.Data.ToString());

                    if (string.IsNullOrEmpty(message.ReceiverId) && string.IsNullOrEmpty(message.ChannelId) && string.IsNullOrEmpty(message.SenderId)) return;

                    if (string.IsNullOrEmpty(message.ReceiverId))
                    {
                        List<ChannelEndpoint> endpoints = new List<ChannelEndpoint>();
                        if (string.IsNullOrEmpty(message.UserId))
                        {
                            endpoints = ModuleDataExchanger.GetReceivers(message.ChannelId, message.SessionId);
                        }
                        else
                        {
                            endpoints = ModuleDataExchanger.GetReceivers(message.ChannelId, message.SessionId, message.UserId);
                        }
                        
                        var endpointsWithSenderId = endpoints.Where(endpoint => !string.IsNullOrEmpty(endpoint.SessionId)).ToList();

                        if (endpoints == null || !endpoints.Any())
                        {
                            if (!string.IsNullOrEmpty(message.UserId))
                            {
                                if (endpoints.Any())
                                {
                                    endpoints.ForEach(endPoint =>
                                    {
                                        senderSocketSession = Sessions.Sessions.FirstOrDefault(session => session.Context.QueryString["EndpointId"] == endPoint.EndPointId);

                                        if (senderSocketSession != null)
                                        {
                                            message.SenderId = message.SenderId;
                                            message.CommunicationStatus = CommunicationStatus.NoReceiver;
                                            var jsonMessage = Newtonsoft.Json.JsonConvert.SerializeObject(message);
                                            Sessions.SendTo(jsonMessage, senderSocketSession.ID);
                                        }
                                    });

                                }
                            }
                            else
                            {
                                senderSocketSession = Sessions.Sessions.FirstOrDefault(session => session.Context.QueryString["EndpointId"] == message.SenderId && session.Context.QueryString["Session"] == message.SessionId);

                                if (senderSocketSession != null)
                                {
                                    message.CommunicationStatus = CommunicationStatus.NoReceiver;
                                    var jsonMessage = Newtonsoft.Json.JsonConvert.SerializeObject(message);
                                    Sessions.SendTo(jsonMessage, senderSocketSession.ID);
                                }
                            }
                            

                        }
                        else if (endpointsWithSenderId != null && endpointsWithSenderId.Any())
                        {

                            endpointsWithSenderId.ForEach(endpoint =>
                            {
                                if (string.IsNullOrEmpty(message.UserId))
                                {
                                    senderSocketSession = Sessions.Sessions.FirstOrDefault(session => session.Context.QueryString["EndpointId"] == endpoint.EndPointId && session.Context.QueryString["Session"] == message.SessionId);
                                }
                                else
                                {
                                    senderSocketSession = Sessions.Sessions.FirstOrDefault(session => session.Context.QueryString["EndpointId"] == message.ReceiverId);
                                }
                                

                                if (senderSocketSession != null)
                                {
                                    message.ReceiverId = endpoint.EndPointId;
                                    message.CommunicationStatus = CommunicationStatus.MessageDelivered;
                                    var jsonMessage = Newtonsoft.Json.JsonConvert.SerializeObject(message);
                                    Sessions.SendTo(jsonMessage, senderSocketSession.ID);
                                }
                            });
                            

                            
                        }
                    }
                    else
                    {

                        if (string.IsNullOrEmpty(message.UserId))
                        {
                            senderSocketSession = Sessions.Sessions.FirstOrDefault(session => session.Context.QueryString["EndpointId"] == message.ReceiverId && session.Context.QueryString["Session"] == message.SessionId);
                        }
                        else
                        {
                            senderSocketSession = Sessions.Sessions.FirstOrDefault(session => session.Context.QueryString["EndpointId"] == message.ReceiverId);
                        }
                        
                        if (senderSocketSession != null)
                        {
                            message.CommunicationStatus = CommunicationStatus.MessageDelivered;
                            var jsonMessage = Newtonsoft.Json.JsonConvert.SerializeObject(message);
                            Sessions.SendTo(jsonMessage, senderSocketSession.ID);
                        }
                    }
                    

                }
                catch (Exception ex)
                {

                }
                
                
            }
            catch (Exception ex) { }
            return;

        }

        /// <summary>
        /// If websocket is opening
        /// </summary>
        protected override void OnOpen()
        {
            
        }

        protected override void OnError(ErrorEventArgs e)
        {

        }

        

        public ComServiceAgent()
        {

        }
    }
}
