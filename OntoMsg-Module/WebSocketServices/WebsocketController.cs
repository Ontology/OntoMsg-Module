﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoMsg_Module.WebSocketServices
{
    public class WebsocketController
    {
        public string Key { get; set; }
        public string Name { get; set; }
        public string ClassName { get; set; }
    }
}
