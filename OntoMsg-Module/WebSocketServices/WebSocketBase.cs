﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OntoMsg_Module.WebSocketServices
{
    public static class WebSocketBase
    {
        private static clsLocalConfig localConfig;

        public static string MessageType_Change
        {
            get
            {
                if (localConfig == null)
                {
                    Initialize();
                    
                }
                return localConfig.OItem_object_change.Name;

            }
        }

        public static string MessageType_Command
        {
            get
            {
                if (localConfig == null)
                {
                    Initialize();

                }
                return localConfig.OItem_object_command.Name;

            }
        }

        public static string MessageType_Model
        {
            get
            {
                if (localConfig == null)
                {
                    Initialize();

                }
                return localConfig.OItem_object_model.Name;

            }
        }

        public static string MessageType_Event
        {
            get
            {
                if (localConfig == null)
                {
                    Initialize();

                }
                return localConfig.OItem_object_event.Name;

            }
        }


        public static string Property_MessageType
        {
            get
            {
                if (localConfig == null)
                {
                    Initialize();

                }
                return localConfig.OItem_object_messagetype.Name;

            }
        }

        public static string Property_PropertyName
        {
            get
            {
                if (localConfig == null)
                {
                    Initialize();

                }
                return localConfig.OItem_object_propertyname.Name;

            }
        }

        public static string QueryParam_Session
        {
            get
            {
                if (localConfig == null)
                {
                    Initialize();
                }
                return localConfig.OItem_object_session.Name;
            }
        }

        public static string QueryParam_Module
        {
            get
            {
                if (localConfig == null)
                {
                    Initialize();
                }
                return localConfig.OItem_object_module.Name;
            }
        }

        public static string QueryParam_Controller
        {
            get
            {
                if (localConfig == null)
                {
                    Initialize();
                }
                return localConfig.OItem_object_controller.Name;
            }
        }

        

        public static string QueryParam_Endpoint
        {
            get
            {
                if (localConfig == null)
                {
                    Initialize();
                }
                return localConfig.OItem_object_endpoint.Name;
            }
        }

        public static string QueryParam_View
        {
            get
            {
                if (localConfig == null)
                {
                    Initialize();
                }
                return localConfig.OItem_object_view.Name;
            }
        }

        private static List<clsOntologyItem> anonymousItems;
        public static List<clsOntologyItem> AnonymousItems
        {
            get
            {
                if (localConfig == null)
                {
                    Initialize();
                }

                return anonymousItems;
            }
        }

        private static string comServerIp;
        public static string ComServerIP
        {
            get
            {
                return comServerIp;
            }
            set
            {
                comServerIp = value;
            }
        }

        private static int comServerPort;
        public static int ComServerPort
        {
            get
            {
                return comServerPort;
            }
            set
            {
                comServerPort = value;
            }
        }

        private static object locker = new object();

        private static void Initialize(Globals globals = null)
        {
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(globals == null ? new Globals() : globals);
                LocalConfigManager.AddLocalConfig(localConfig);
            }
            GetAnonymousItems();
        }

        public static List<clsOntologyItem> GetAnonymousItems(Globals globals = null)
        {
            if (localConfig == null)
            {
                Initialize(globals);
            }
            anonymousItems = new List<clsOntologyItem>();
            lock(locker)
            {
                var searchAnonymousItems = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = localConfig.OItem_object_baseconfig.GUID,
                        ID_RelationType = localConfig.OItem_relationtype_anonymous.GUID
                    }
                };

                var dbReader = new OntologyModDBConnector(localConfig.Globals);
                var result = dbReader.GetDataObjectRel(searchAnonymousItems);

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    anonymousItems = dbReader.ObjectRels.Select(rel => new clsOntologyItem
                    {
                        GUID = rel.ID_Other,
                        Name = rel.Name_Other,
                        GUID_Parent = rel.ID_Parent_Other,
                        Type = rel.Ontology
                    }).ToList();
                    
                }

                
            }

            return anonymousItems;
            
        }
    }
}
