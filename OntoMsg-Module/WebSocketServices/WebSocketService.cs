﻿using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoMsg_Module.WebSocketServices
{
    public class WebSocketService
    {
        public bool IsActive { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public string AssemblyPath { get; set; }
        public string WebSocketPath { get; set; }
        public List<WebsocketController> WebSocketControllers { get; set; }
        public WebsocketServiceAgent WebSocketServiceAgent { get; set; }
    }
}
