﻿using Microsoft.AspNet.SignalR.Client;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace OntoMsg_Module.WebSocketServices
{
    public delegate void ReceivedMessageHandler(InterServiceMessage message);
    public class WinformsWebsocketController
    {
        private Globals globals;
        private IHubProxy hubProxy;
        public event ReceivedMessageHandler ReceivedMessage;
        private ChannelEndpoint endpoint;

        public async Task<clsOntologyItem> Connect(string url, clsOntologyItem userItem, string password)
        {
            var taskResult = await Task.Run<clsOntologyItem>(async() =>
            {
                var result = globals.LState_Success.Clone();

                try
                {
                    var hubConnection = new HubConnection(url);

                    hubConnection.Credentials = new NetworkCredential(userItem.Name, password);
                    hubProxy = hubConnection.CreateHubProxy("ModuleComHub");
                    hubProxy.Subscribe("addNewMessageToPage");

                    hubProxy.On<string, InterServiceMessage>("addNewMessageToPage", (name, message) =>
                    {
                        ReceivedMessage?.Invoke(message);
                    });

                    hubConnection.Start().Wait();

                    endpoint = new ChannelEndpoint
                    {
                        ChannelTypeId = Channels.SelectedClassNode,
                        EndPointId = Guid.NewGuid().ToString(),
                        EndpointType = EndpointType.Receiver,
                        SessionId = Guid.NewGuid().ToString(),
                        UserId = userItem.GUID
                    };

                    await hubProxy.Invoke("RegisterEndpoint", endpoint);
                }
                catch (Exception ex)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"Error while connecting to Websocket-hub: {ex.Message}";
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SendMessage(InterServiceMessage message)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                try
                {
                    hubProxy.Invoke("SendInterServiceMessage", message);
                }
                catch (Exception ex)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"Error while sending message: {ex.Message}";
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public WinformsWebsocketController(Globals globals)
        {
            this.globals = globals;
        }

    }
}
