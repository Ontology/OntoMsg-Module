﻿using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSocketSharp;

namespace OntoMsg_Module.WebSocketServices
{
    public delegate void ChangedConnectionState(bool isConnected);
    public delegate void ChangedLoginState(bool isLoggedIn);

    public class ProxyController
    {
        private WebSocket webSocketClient;
        private bool loginSuccess;

        public bool IsLoggedIn
        {
            get
            {
                return loginSuccess;
            }
        }

        private bool isOpened;
        private clsLogStates logStates = new clsLogStates();
        private string userName;
        private string group;
        private string password;

        public bool DoSend
        {
            get;set;
        }

        public event ChangedConnectionState changedConnectionState;
        public event ChangedLoginState changedLoginState;

        public ProxyController(string server, int port, string userName, string group, string password)
        {
            this.userName = userName;
            this.group = group;
            this.password = password;

            webSocketClient = new WebSocket("wss://" + server + ":" + port.ToString() + "/WinFormsProxyController.WinFormsProxyController?Module=5cc0ebc8235f49849997179b057918b1&Controller=e96dcb5faba3439ba81b229914429fd0&Endpoint=" + Guid.NewGuid().ToString() + "&Session=" + Guid.NewGuid().ToString());
            webSocketClient.SslConfiguration.ClientCertificates = new System.Security.Cryptography.X509Certificates.X509CertificateCollection();
            webSocketClient.SslConfiguration.ClientCertificates.Add(new System.Security.Cryptography.X509Certificates.X509Certificate2("ontosockcert.pfx", ""));

            webSocketClient.OnClose += WebSocketClient_OnClose;
            webSocketClient.OnError += WebSocketClient_OnError;
            webSocketClient.OnMessage += WebSocketClient_OnMessage;
            webSocketClient.OnOpen += WebSocketClient_OnOpen;

            try
            {
                webSocketClient.Connect();
                
                changedConnectionState?.Invoke(true);
                
            }
            catch (Exception)
            {
                changedConnectionState?.Invoke(false);

            }
        }

        private void WebSocketClient_OnOpen(object sender, EventArgs e)
        {
            isOpened = true;

            changedConnectionState?.Invoke(isOpened);
            

            var dictLogin = new Dictionary<string, string>();

            dictLogin.Add("MessageType", "Change");
            dictLogin.Add("PropertyName", "Username");
            dictLogin.Add("Username", userName);

            var sendItem = Newtonsoft.Json.JsonConvert.SerializeObject(dictLogin);
            webSocketClient.Send(sendItem);


            dictLogin.Clear();

            dictLogin.Add("MessageType", "Change");
            dictLogin.Add("PropertyName", "Group");
            dictLogin.Add("Group", group);

            sendItem = Newtonsoft.Json.JsonConvert.SerializeObject(dictLogin);
            webSocketClient.Send(sendItem);

            dictLogin.Clear();

            dictLogin.Add("MessageType", "Change");
            dictLogin.Add("PropertyName", "Password");
            dictLogin.Add("Password", password);

            sendItem = Newtonsoft.Json.JsonConvert.SerializeObject(dictLogin);
            webSocketClient.Send(sendItem);
        }

        private void WebSocketClient_OnMessage(object sender, MessageEventArgs e)
        {
            try
            {
                var dictList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(e.Data);

                if (dictList.Any())
                {
                    dictList.ForEach(dict =>
                    {
                        if (dict["ViewItemId"].ToString() == "IsSuccessful_Login")
                        {
                            if ((bool)dict["LastValue"])
                            {
                                loginSuccess = (bool)dict["LastValue"];
                            }
                        }
                    });
                }

                if (loginSuccess)
                {
                    changedLoginState?.Invoke(true);
                }
                else
                {
                    changedLoginState?.Invoke(false);
                }
                
                
            }
            catch (Exception)
            {
                changedLoginState?.Invoke(false);

            }
        }

        private void WebSocketClient_OnError(object sender, ErrorEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void WebSocketClient_OnClose(object sender, CloseEventArgs e)
        {
            throw new NotImplementedException();
        }

        public clsOntologyItem SelectedItem(clsOntologyItem oItem )
        {
            if (!DoSend)
            {
                return logStates.LogState_Nothing.Clone();
            }

            var result = logStates.LogState_Success.Clone();

            if (isOpened && IsLoggedIn)
            {
                var dictSend = new Dictionary<string, object>();

                dictSend.Add("MessageType", "ViewItem");

                var sendItems = new List<ViewItem>();

                sendItems.Add(new ViewItem {
                    ViewItemId = "OItem",
                    ViewItemClass = "Other",
                    ViewItemType = "Other",
                    ViewItemValue = oItem });

                dictSend.Add("Items", sendItems);

                var jsonSend = Newtonsoft.Json.JsonConvert.SerializeObject(dictSend);
                webSocketClient.Send(jsonSend);
            }
            else
            {
                result = logStates.LogState_Nothing.Clone();
            }

            return result;
        }

    }
}
