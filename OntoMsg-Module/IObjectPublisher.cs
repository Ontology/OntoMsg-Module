﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoMsg_Module
{
    
    public interface IObjectPublisher
    {
        event MessageManager.ShowObjectsOfClass _showObjectsOfClass;
        event MessageManager.ExchangeOItem _exchangeOItem;
        SubscriberType SubscriberType { get; }
        string IdPublisherType { get; }
    }
}
