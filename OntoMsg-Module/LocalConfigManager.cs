﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyAppDBConnector;
using OntologyClasses.Interfaces;
using OntoMsg_Module.WebSocketServices;
using OntologyClasses.BaseClasses;

namespace OntoMsg_Module
{
    public static class LocalConfigManager
    {
        private static object itemLocker = new object();
        private static List<ILocalConfig> LocalConfigList { get; set; }

        public static bool AddLocalConfig(ILocalConfig localConfig)
        {
            lock(itemLocker)
            {
                if (LocalConfigList == null)
                {
                    LocalConfigList = new List<ILocalConfig>();
                }

                if (LocalConfigList.All(locconf => locconf.IdLocalConfig != localConfig.IdLocalConfig))
                {
                    LocalConfigList.Add(localConfig);
                }
            }
            
            return true;
            
        }

        public static ILocalConfig GetLocalConfig(string idLocalConfig)
        {
            lock (itemLocker)
            {
                if (LocalConfigList != null)
                {
                    var localConfig = LocalConfigList.FirstOrDefault(lc => lc.IdLocalConfig == idLocalConfig);
                    return localConfig;
                }
            }
            

            return null;
        }

    }
}
