﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoMsg_Module
{
    [Serializable]
    public class clsApplyItem
    {
        public string IdItem { get; set; }
        public string NameItem { get; set; }
        public string IdItemParent { get; set; }
        public string Type { get; set; }
    }
}
