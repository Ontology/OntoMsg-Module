﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OntologyClasses.BaseClasses;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using OntologyAppDBConnector;
using ImportExport_Module;
using OntologyClasses.Interfaces;

namespace OntoMsg_Module
{
    public class clsLocalConfig : ILocalConfig
    {
        private const string cstrID_Ontology = "e1631fc3cd3740e4bd933e21a9b6e9ba";
        private ImportWorker objImport;

        public Globals Globals { get; set; }

        private clsOntologyItem objOItem_DevConfig = new clsOntologyItem();
        public clsOntologyItem OItem_BaseConfig { get; set; }

        private OntologyModDBConnector objDBLevel_Config1;
        private OntologyModDBConnector objDBLevel_Config2;

        // Classes
        public clsOntologyItem OItem_class_message_server { get; set; }
        public clsOntologyItem OItem_class_port { get; set; }
        public clsOntologyItem OItem_class_server { get; set; }
        public clsOntologyItem OItem_class_server_port { get; set; }

        // Objects
        public clsOntologyItem OItem_object_baseconfig { get; set; }
        public clsOntologyItem OItem_object_change { get; set; }
        public clsOntologyItem OItem_object_command { get; set; }
        public clsOntologyItem OItem_object_messagetype { get; set; }
        public clsOntologyItem OItem_object_model { get; set; }
        public clsOntologyItem OItem_object_propertyname { get; set; }
        public clsOntologyItem OItem_object_session { get; set; }
        public clsOntologyItem OItem_object_event { get; set; }
        public clsOntologyItem OItem_object_controller { get; set; }
        public clsOntologyItem OItem_object_module { get; set; }
        public clsOntologyItem OItem_object_login { get; set; }
        public clsOntologyItem OItem_object_endpoint { get; set; }
        public clsOntologyItem OItem_object_addclassnode { get; set; }
        public clsOntologyItem OItem_object_selectedclassnode { get; set; }
        public clsOntologyItem OItem_object_selectclassnode { get; set; }
        public clsOntologyItem OItem_object_removeclassnode { get; set; }
        public clsOntologyItem OItem_object_markclassnodes { get; set; }
        public clsOntologyItem OItem_object_changeclassnode { get; set; }
        public clsOntologyItem OItem_object_selectedobject { get; set; }
        public clsOntologyItem OItem_object_view { get; set; }
        public clsOntologyItem OItem_object_fileupload { get; set; }
        public clsOntologyItem OItem_object_selectedrelationnode { get; set; }
        public clsOntologyItem OItem_object_viewready { get; set; }

        // RelationTypes
        public clsOntologyItem OItem_relationtype_belonging_source { get; set; }
        public clsOntologyItem OItem_relationtype_is_defined_by { get; set; }
        public clsOntologyItem OItem_relationtype_uses { get; set; }
        public clsOntologyItem OItem_relationtype_datapath { get; set; }
        public clsOntologyItem OItem_relationtype_anonymous { get; set; }



        private void get_Data_DevelopmentConfig()
        {
            var objORL_Ontology_To_OntolgyItems = new List<clsObjectRel> {new clsObjectRel {ID_Object = cstrID_Ontology, 
                                                                                             ID_RelationType = Globals.RelationType_contains.GUID, 
                                                                                             ID_Parent_Other = Globals.Class_OntologyItems.GUID}};

            var objOItem_Result = objDBLevel_Config1.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
            if (objOItem_Result.GUID == Globals.LState_Success.GUID)
            {
                if (objDBLevel_Config1.ObjectRels.Any())
                {

                    objORL_Ontology_To_OntolgyItems = objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                    {
                        ID_Object = oi.ID_Other,
                        ID_RelationType = Globals.RelationType_belongingAttribute.GUID
                    }).ToList();

                    objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                    {
                        ID_Object = oi.ID_Other,
                        ID_RelationType = Globals.RelationType_belongingClass.GUID
                    }));
                    objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                    {
                        ID_Object = oi.ID_Other,
                        ID_RelationType = Globals.RelationType_belongingObject.GUID
                    }));
                    objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                    {
                        ID_Object = oi.ID_Other,
                        ID_RelationType = Globals.RelationType_belongingRelationType.GUID
                    }));

                    objOItem_Result = objDBLevel_Config2.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
                    if (objOItem_Result.GUID == Globals.LState_Success.GUID)
                    {
                        if (!objDBLevel_Config2.ObjectRels.Any())
                        {
                            throw new Exception("Config-Error");
                        }
                    }
                    else
                    {
                        throw new Exception("Config-Error");
                    }
                }
                else
                {
                    throw new Exception("Config-Error");
                }

            }

        }

        public clsLocalConfig()
        {
            Globals = new Globals();
            set_DBConnection();
            get_Config();
        }

        public clsLocalConfig(Globals Globals)
        {
            this.Globals = Globals;
            set_DBConnection();
            get_Config();
        }

        private void set_DBConnection()
        {
            objDBLevel_Config1 = new OntologyModDBConnector(Globals);
            objDBLevel_Config2 = new OntologyModDBConnector(Globals);
            objImport = new ImportWorker(Globals);
        }

        private void get_Config()
        {
            try
            {
                get_Data_DevelopmentConfig();
                get_Config_AttributeTypes();
                get_Config_RelationTypes();
                get_Config_Classes();
                get_Config_Objects();
            }
            catch (Exception ex)
            {
                var objAssembly = Assembly.GetExecutingAssembly();
                AssemblyTitleAttribute[] objCustomAttributes = (AssemblyTitleAttribute[])objAssembly.GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                var strTitle = "Unbekannt";
                if (objCustomAttributes.Length == 1)
                {
                    strTitle = objCustomAttributes.First().Title;
                }
                if (MessageBox.Show(strTitle + ": Die notwendigen Basisdaten konnten nicht geladen werden! Soll versucht werden, sie in der Datenbank " +
                          Globals.Index + "@" + Globals.Server + " zu erzeugen?", "Datenstrukturen", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    var objOItem_Result = objImport.ImportTemplates(objAssembly);
                    if (objOItem_Result.GUID != Globals.LState_Error.GUID)
                    {
                        get_Data_DevelopmentConfig();
                        get_Config_AttributeTypes();
                        get_Config_RelationTypes();
                        get_Config_Classes();
                        get_Config_Objects();
                    }
                    else
                    {
                        throw new Exception("Config not importable");
                    }
                }
                else
                {
                    Environment.Exit(0);
                }
            }
        }

        private void get_Config_AttributeTypes()
        {

        }

        private void get_Config_RelationTypes()
        {
            var objOList_relationtype_anonymous = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "relationtype_anonymous".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                   select objRef).ToList();

            if (objOList_relationtype_anonymous.Any())
            {
                OItem_relationtype_anonymous = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_anonymous.First().ID_Other,
                    Name = objOList_relationtype_anonymous.First().Name_Other,
                    GUID_Parent = objOList_relationtype_anonymous.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_belonging_source = (from objOItem in objDBLevel_Config1.ObjectRels
                                                          where objOItem.ID_Object == cstrID_Ontology
                                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                          where objRef.Name_Object.ToLower() == "relationtype_belonging_source".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                          select objRef).ToList();

            if (objOList_relationtype_belonging_source.Any())
            {
                OItem_relationtype_belonging_source = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_belonging_source.First().ID_Other,
                    Name = objOList_relationtype_belonging_source.First().Name_Other,
                    GUID_Parent = objOList_relationtype_belonging_source.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_is_defined_by = (from objOItem in objDBLevel_Config1.ObjectRels
                                                       where objOItem.ID_Object == cstrID_Ontology
                                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                       where objRef.Name_Object.ToLower() == "relationtype_is_defined_by".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                       select objRef).ToList();

            if (objOList_relationtype_is_defined_by.Any())
            {
                OItem_relationtype_is_defined_by = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_is_defined_by.First().ID_Other,
                    Name = objOList_relationtype_is_defined_by.First().Name_Other,
                    GUID_Parent = objOList_relationtype_is_defined_by.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_uses = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "relationtype_uses".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                              select objRef).ToList();

            if (objOList_relationtype_uses.Any())
            {
                OItem_relationtype_uses = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_uses.First().ID_Other,
                    Name = objOList_relationtype_uses.First().Name_Other,
                    GUID_Parent = objOList_relationtype_uses.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }


        }

        private void get_Config_Objects()
        {
            var objOList_object_viewready = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "object_viewready".ToLower() && objRef.Ontology == Globals.Type_Object
                                             select objRef).ToList();

            if (objOList_object_viewready.Any())
            {
                OItem_object_viewready = new clsOntologyItem()
                {
                    GUID = objOList_object_viewready.First().ID_Other,
                    Name = objOList_object_viewready.First().Name_Other,
                    GUID_Parent = objOList_object_viewready.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_selectedrelationnode = (from objOItem in objDBLevel_Config1.ObjectRels
                                                        where objOItem.ID_Object == cstrID_Ontology
                                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                        where objRef.Name_Object.ToLower() == "object_selectedrelationnode".ToLower() && objRef.Ontology == Globals.Type_Object
                                                        select objRef).ToList();

            if (objOList_object_selectedrelationnode.Any())
            {
                OItem_object_selectedrelationnode = new clsOntologyItem()
                {
                    GUID = objOList_object_selectedrelationnode.First().ID_Other,
                    Name = objOList_object_selectedrelationnode.First().Name_Other,
                    GUID_Parent = objOList_object_selectedrelationnode.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_fileupload = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "object_fileupload".ToLower() && objRef.Ontology == Globals.Type_Object
                                              select objRef).ToList();

            if (objOList_object_fileupload.Any())
            {
                OItem_object_fileupload = new clsOntologyItem()
                {
                    GUID = objOList_object_fileupload.First().ID_Other,
                    Name = objOList_object_fileupload.First().Name_Other,
                    GUID_Parent = objOList_object_fileupload.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_view = (from objOItem in objDBLevel_Config1.ObjectRels
                                        where objOItem.ID_Object == cstrID_Ontology
                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                        where objRef.Name_Object.ToLower() == "object_view".ToLower() && objRef.Ontology == Globals.Type_Object
                                        select objRef).ToList();

            if (objOList_object_view.Any())
            {
                OItem_object_view = new clsOntologyItem()
                {
                    GUID = objOList_object_view.First().ID_Other,
                    Name = objOList_object_view.First().Name_Other,
                    GUID_Parent = objOList_object_view.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_selectedobject = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "object_selectedobject".ToLower() && objRef.Ontology == Globals.Type_Object
                                                  select objRef).ToList();

            if (objOList_object_selectedobject.Any())
            {
                OItem_object_selectedobject = new clsOntologyItem()
                {
                    GUID = objOList_object_selectedobject.First().ID_Other,
                    Name = objOList_object_selectedobject.First().Name_Other,
                    GUID_Parent = objOList_object_selectedobject.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }


            var objOList_object_addclassnode = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "object_addclassnode".ToLower() && objRef.Ontology == Globals.Type_Object
                                                select objRef).ToList();

            if (objOList_object_addclassnode.Any())
            {
                OItem_object_addclassnode = new clsOntologyItem()
                {
                    GUID = objOList_object_addclassnode.First().ID_Other,
                    Name = objOList_object_addclassnode.First().Name_Other,
                    GUID_Parent = objOList_object_addclassnode.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_selectedclassnode = (from objOItem in objDBLevel_Config1.ObjectRels
                                                     where objOItem.ID_Object == cstrID_Ontology
                                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                     where objRef.Name_Object.ToLower() == "object_selectedclassnode".ToLower() && objRef.Ontology == Globals.Type_Object
                                                     select objRef).ToList();

            if (objOList_object_selectedclassnode.Any())
            {
                OItem_object_selectedclassnode = new clsOntologyItem()
                {
                    GUID = objOList_object_selectedclassnode.First().ID_Other,
                    Name = objOList_object_selectedclassnode.First().Name_Other,
                    GUID_Parent = objOList_object_selectedclassnode.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_selectclassnode = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "object_selectclassnode".ToLower() && objRef.Ontology == Globals.Type_Object
                                                   select objRef).ToList();

            if (objOList_object_selectclassnode.Any())
            {
                OItem_object_selectclassnode = new clsOntologyItem()
                {
                    GUID = objOList_object_selectclassnode.First().ID_Other,
                    Name = objOList_object_selectclassnode.First().Name_Other,
                    GUID_Parent = objOList_object_selectclassnode.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_removeclassnode = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "object_removeclassnode".ToLower() && objRef.Ontology == Globals.Type_Object
                                                   select objRef).ToList();

            if (objOList_object_removeclassnode.Any())
            {
                OItem_object_removeclassnode = new clsOntologyItem()
                {
                    GUID = objOList_object_removeclassnode.First().ID_Other,
                    Name = objOList_object_removeclassnode.First().Name_Other,
                    GUID_Parent = objOList_object_removeclassnode.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_markclassnodes = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "object_markclassnodes".ToLower() && objRef.Ontology == Globals.Type_Object
                                                  select objRef).ToList();

            if (objOList_object_markclassnodes.Any())
            {
                OItem_object_markclassnodes = new clsOntologyItem()
                {
                    GUID = objOList_object_markclassnodes.First().ID_Other,
                    Name = objOList_object_markclassnodes.First().Name_Other,
                    GUID_Parent = objOList_object_markclassnodes.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_changeclassnode = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "object_changeclassnode".ToLower() && objRef.Ontology == Globals.Type_Object
                                                   select objRef).ToList();

            if (objOList_object_changeclassnode.Any())
            {
                OItem_object_changeclassnode = new clsOntologyItem()
                {
                    GUID = objOList_object_changeclassnode.First().ID_Other,
                    Name = objOList_object_changeclassnode.First().Name_Other,
                    GUID_Parent = objOList_object_changeclassnode.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_endpoint = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "object_endpoint".ToLower() && objRef.Ontology == Globals.Type_Object
                                            select objRef).ToList();

            if (objOList_object_endpoint.Any())
            {
                OItem_object_endpoint = new clsOntologyItem()
                {
                    GUID = objOList_object_endpoint.First().ID_Other,
                    Name = objOList_object_endpoint.First().Name_Other,
                    GUID_Parent = objOList_object_endpoint.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_login = (from objOItem in objDBLevel_Config1.ObjectRels
                                         where objOItem.ID_Object == cstrID_Ontology
                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                         where objRef.Name_Object.ToLower() == "object_login".ToLower() && objRef.Ontology == Globals.Type_Object
                                         select objRef).ToList();

            if (objOList_object_login.Any())
            {
                OItem_object_login = new clsOntologyItem()
                {
                    GUID = objOList_object_login.First().ID_Other,
                    Name = objOList_object_login.First().Name_Other,
                    GUID_Parent = objOList_object_login.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_controller = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "object_controller".ToLower() && objRef.Ontology == Globals.Type_Object
                                              select objRef).ToList();

            if (objOList_object_controller.Any())
            {
                OItem_object_controller = new clsOntologyItem()
                {
                    GUID = objOList_object_controller.First().ID_Other,
                    Name = objOList_object_controller.First().Name_Other,
                    GUID_Parent = objOList_object_controller.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_module = (from objOItem in objDBLevel_Config1.ObjectRels
                                          where objOItem.ID_Object == cstrID_Ontology
                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                          where objRef.Name_Object.ToLower() == "object_module".ToLower() && objRef.Ontology == Globals.Type_Object
                                          select objRef).ToList();

            if (objOList_object_module.Any())
            {
                OItem_object_module = new clsOntologyItem()
                {
                    GUID = objOList_object_module.First().ID_Other,
                    Name = objOList_object_module.First().Name_Other,
                    GUID_Parent = objOList_object_module.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_event = (from objOItem in objDBLevel_Config1.ObjectRels
                                         where objOItem.ID_Object == cstrID_Ontology
                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                         where objRef.Name_Object.ToLower() == "object_event".ToLower() && objRef.Ontology == Globals.Type_Object
                                         select objRef).ToList();

            if (objOList_object_event.Any())
            {
                OItem_object_event = new clsOntologyItem()
                {
                    GUID = objOList_object_event.First().ID_Other,
                    Name = objOList_object_event.First().Name_Other,
                    GUID_Parent = objOList_object_event.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_session = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "object_session".ToLower() && objRef.Ontology == Globals.Type_Object
                                           select objRef).ToList();

            if (objOList_object_session.Any())
            {
                OItem_object_session = new clsOntologyItem()
                {
                    GUID = objOList_object_session.First().ID_Other,
                    Name = objOList_object_session.First().Name_Other,
                    GUID_Parent = objOList_object_session.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_change = (from objOItem in objDBLevel_Config1.ObjectRels
                                          where objOItem.ID_Object == cstrID_Ontology
                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                          where objRef.Name_Object.ToLower() == "object_change".ToLower() && objRef.Ontology == Globals.Type_Object
                                          select objRef).ToList();

            if (objOList_object_change.Any())
            {
                OItem_object_change = new clsOntologyItem()
                {
                    GUID = objOList_object_change.First().ID_Other,
                    Name = objOList_object_change.First().Name_Other,
                    GUID_Parent = objOList_object_change.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_command = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "object_command".ToLower() && objRef.Ontology == Globals.Type_Object
                                           select objRef).ToList();

            if (objOList_object_command.Any())
            {
                OItem_object_command = new clsOntologyItem()
                {
                    GUID = objOList_object_command.First().ID_Other,
                    Name = objOList_object_command.First().Name_Other,
                    GUID_Parent = objOList_object_command.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_messagetype = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "object_messagetype".ToLower() && objRef.Ontology == Globals.Type_Object
                                               select objRef).ToList();

            if (objOList_object_messagetype.Any())
            {
                OItem_object_messagetype = new clsOntologyItem()
                {
                    GUID = objOList_object_messagetype.First().ID_Other,
                    Name = objOList_object_messagetype.First().Name_Other,
                    GUID_Parent = objOList_object_messagetype.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_model = (from objOItem in objDBLevel_Config1.ObjectRels
                                         where objOItem.ID_Object == cstrID_Ontology
                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                         where objRef.Name_Object.ToLower() == "object_model".ToLower() && objRef.Ontology == Globals.Type_Object
                                         select objRef).ToList();

            if (objOList_object_model.Any())
            {
                OItem_object_model = new clsOntologyItem()
                {
                    GUID = objOList_object_model.First().ID_Other,
                    Name = objOList_object_model.First().Name_Other,
                    GUID_Parent = objOList_object_model.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_propertyname = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "object_propertyname".ToLower() && objRef.Ontology == Globals.Type_Object
                                                select objRef).ToList();

            if (objOList_object_propertyname.Any())
            {
                OItem_object_propertyname = new clsOntologyItem()
                {
                    GUID = objOList_object_propertyname.First().ID_Other,
                    Name = objOList_object_propertyname.First().Name_Other,
                    GUID_Parent = objOList_object_propertyname.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_baseconfig = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "object_baseconfig".ToLower() && objRef.Ontology == Globals.Type_Object
                                              select objRef).ToList();

            if (objOList_object_baseconfig.Any())
            {
                OItem_object_baseconfig = new clsOntologyItem()
                {
                    GUID = objOList_object_baseconfig.First().ID_Other,
                    Name = objOList_object_baseconfig.First().Name_Other,
                    GUID_Parent = objOList_object_baseconfig.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }


        }

        private void get_Config_Classes()
        {
            var objOList_class_message_server = (from objOItem in objDBLevel_Config1.ObjectRels
                                                 where objOItem.ID_Object == cstrID_Ontology
                                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                 where objRef.Name_Object.ToLower() == "class_message_server".ToLower() && objRef.Ontology == Globals.Type_Class
                                                 select objRef).ToList();

            if (objOList_class_message_server.Any())
            {
                OItem_class_message_server = new clsOntologyItem()
                {
                    GUID = objOList_class_message_server.First().ID_Other,
                    Name = objOList_class_message_server.First().Name_Other,
                    GUID_Parent = objOList_class_message_server.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_port = (from objOItem in objDBLevel_Config1.ObjectRels
                                       where objOItem.ID_Object == cstrID_Ontology
                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                       where objRef.Name_Object.ToLower() == "class_port".ToLower() && objRef.Ontology == Globals.Type_Class
                                       select objRef).ToList();

            if (objOList_class_port.Any())
            {
                OItem_class_port = new clsOntologyItem()
                {
                    GUID = objOList_class_port.First().ID_Other,
                    Name = objOList_class_port.First().Name_Other,
                    GUID_Parent = objOList_class_port.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_server = (from objOItem in objDBLevel_Config1.ObjectRels
                                         where objOItem.ID_Object == cstrID_Ontology
                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                         where objRef.Name_Object.ToLower() == "class_server".ToLower() && objRef.Ontology == Globals.Type_Class
                                         select objRef).ToList();

            if (objOList_class_server.Any())
            {
                OItem_class_server = new clsOntologyItem()
                {
                    GUID = objOList_class_server.First().ID_Other,
                    Name = objOList_class_server.First().Name_Other,
                    GUID_Parent = objOList_class_server.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_server_port = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "class_server_port".ToLower() && objRef.Ontology == Globals.Type_Class
                                              select objRef).ToList();

            if (objOList_class_server_port.Any())
            {
                OItem_class_server_port = new clsOntologyItem()
                {
                    GUID = objOList_class_server_port.First().ID_Other,
                    Name = objOList_class_server_port.First().Name_Other,
                    GUID_Parent = objOList_class_server_port.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }


        }

        public string IdLocalConfig
        {
            get
            {
                var attrib =
                    Assembly.GetExecutingAssembly()
                        .GetCustomAttributes(true)
                        .FirstOrDefault(objAttribute=>objAttribute is GuidAttribute);
                if (attrib != null)
                {
                    return ((GuidAttribute) attrib).Value;
                }
                else
                {
                    return null;
                } 
            }
        }
    }

}