﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoMsg_Module
{
    public class ObjectSubscriber
    {
        private ObjectPublisher publisher;

        public event MessageManager.ShowObjectsOfClass _showObjectsOfClass;
        public event MessageManager.ExchangeOItem _exchangeOItem;

        public string IdPublisherType
        {
            get;
            private set;
        }

        public string IdSubscriber
        {
            get;
            private set;
        }

        public ObjectPublisher Publisher { get; set; }

        public void ClearPublisher()
        {
            this.publisher = null;
        }
        public void SetPublisher(ObjectPublisher publisher)
        {
            this.publisher = publisher;

            if (this.publisher != null)
            {
                this.publisher._exchangeOItem += Publisher__exchangeOItem;
                this.publisher._showObjectsOfClass += Publisher__showObjectsOfClass;
            }
            
        }

        private void Publisher__showObjectsOfClass(OntologyClasses.BaseClasses.clsOntologyItem classItem)
        {
            if (_showObjectsOfClass != null)
            {
                _showObjectsOfClass(classItem);
            }
        }

        private void Publisher__exchangeOItem(OntologyClasses.BaseClasses.clsOntologyItem oItem)
        {
            if (_exchangeOItem != null)
            {
                _exchangeOItem(oItem);
            }
        }

        public ObjectSubscriber(string idSubscriber, string idPublisherType)
        {
            IdSubscriber = string.IsNullOrEmpty(idSubscriber) ? Guid.NewGuid().ToString() : idSubscriber;
            IdPublisherType = idPublisherType;
        }

        public ObjectSubscriber(string idSubscriber,  ObjectPublisher publisher)
        {
            IdSubscriber = string.IsNullOrEmpty(idSubscriber) ? Guid.NewGuid().ToString() : idSubscriber;
            Publisher = publisher;
        }
    }
}
