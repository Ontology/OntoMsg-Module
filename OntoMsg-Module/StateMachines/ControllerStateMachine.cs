﻿using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoMsg_Module.StateMachines
{
    [Flags]
    public enum StateMachineType
    {
        Generic = 0,
        BlockLoadingSelected = 1,
        BlockSelectingSelected = 2
    }

    [Flags]
    public enum GlobalStateMachineState
    {
        Init =	0,
        ItemSelectRequest = 2,
        ItemSelectedPending = 4,
        ItemSelected = 8,
        ItemLoadRequest = 16,
        ItemLoadPending = 32,
        ItemLoaded = 64,
        LoginDone = 128,
        WebSocketOpened	= 256,
        ViewReady = 512,
        Error = 1024
    }

    public delegate void LoadSelectedItem(clsOntologyItem oItemSelected);
    public delegate void UnloadItem(clsOntologyItem oItemLoaded);
    public delegate void LoginSucceeded();
    public delegate void LoginFailed();
    public delegate void OpenedSocket();
    public delegate void ClosedSocket();

    public class ControllerStateMachine : NotifyPropertyChange, IControllerStateMachine
    {
        public string ViewId { get; private set; }
        public string EndpointId { get; private set; }

        private object stateMachineLocker = new object();
        private bool loginSuccessful;
        public bool LoginSuccessful
        {
            get { return loginSuccessful; }
            private set
            {
                loginSuccessful = value;
                RaisePropertyChanged(nameof(LoginSuccessful));
            }
        }

        public bool isControllerListen;
        public bool IsControllerListen
        {
            get { return isControllerListen; }
            set
            {
                isControllerListen = value;
                RaisePropertyChanged(nameof(IsControllerListen));
            }
        }


        public event LoadSelectedItem loadSelectedItem;
        public event UnloadItem unloadItem;
        public event LoginSucceeded loginSucceded;
        public event LoginFailed loginFailed;
        public event OpenedSocket openedSocket;
        public event ClosedSocket closedSocket;

        public StateMachineType  StateMachineType { get; private set; }

        private GlobalStateMachineState globalStateMachineState;
        public GlobalStateMachineState GlobalStateMachineState
        {
            get
            {
                lock(stateMachineLocker)
                {
                    return globalStateMachineState;
                }
                
            }
            private set
            {
                lock(stateMachineLocker)
                {
                    globalStateMachineState = value;
                }
                
                RaisePropertyChanged(nameof(GlobalStateMachineState));
            }
        }

        private clsOntologyItem oItemSelected;
        public clsOntologyItem OItemSelected
        {
            get
            {
                return oItemSelected;
            }
            private set
            {
                oItemSelected = value;
                RaisePropertyChanged(nameof(OItemSelected));
            }
        }

        private clsOntologyItem oItemLoaded;
        public clsOntologyItem OItemLoaded
        {
            get
            {
                return oItemLoaded;
            }
            private set
            {
                oItemLoaded = value;

                RaisePropertyChanged(nameof(OItemLoaded));
                
            }
        }

        public bool IsItemLoaded
        {
            get
            {
                return OItemLoaded != null;
            }
        }

        public bool IsSelectedLoaded
        {
            get
            {
                if (OItemLoaded == null) return false;
                if (OItemSelected == null) return false;

                if (OItemSelected.GUID != OItemSelected.GUID) return false;

                return true;
            }
        }

        public async Task SetItemSelected(clsOntologyItem oItem)
        {
            if (!LoginSuccessful && !IsControllerListen) return;

            SetState(GlobalStateMachineState.ItemSelectRequest);
            if (StateMachineType.HasFlag(StateMachineType.BlockSelectingSelected))
            {
                if (OItemSelected != null && oItem != null && oItem.GUID == OItemSelected.GUID)
                {
                    UnSetState(GlobalStateMachineState.ItemSelectRequest);
                    return;
                }
            }

            OItemSelected = oItem;
            
            
        }

        public void OpenedSocket()
        {
            SetState(GlobalStateMachineState.WebSocketOpened);
            openedSocket?.Invoke();
        }

        public void ClosingSocket()
        {

        }

        public void ClosedSocket()
        {
            UnSetState(GlobalStateMachineState.WebSocketOpened);
            closedSocket?.Invoke();
        }


        public void SetState(GlobalStateMachineState state)
        {
            GlobalStateMachineState |= GlobalStateMachineState.ItemSelectRequest;
            CompleteViewReadyState();
        }

        public void UnSetState(GlobalStateMachineState state)
        {
            GlobalStateMachineState &= ~GlobalStateMachineState.ItemSelectRequest;
            CompleteViewReadyState();
        }

        private void CompleteViewReadyState()
        {
            if (GlobalStateMachineState.HasFlag(GlobalStateMachineState.ItemLoadPending) ||
                GlobalStateMachineState.HasFlag(GlobalStateMachineState.ItemSelectedPending))
            {
                GlobalStateMachineState &= ~GlobalStateMachineState.ViewReady;
                return;
            }

            if (GlobalStateMachineState.HasFlag(GlobalStateMachineState.LoginDone))
            {
                GlobalStateMachineState |= GlobalStateMachineState.ViewReady;
            }
                
        }

        public ControllerStateMachine(StateMachineType stateMachineType, string viewId, string endpointId)
        {
            StateMachineType = stateMachineType;
            ViewId = viewId;
            EndpointId = endpointId;
            this.PropertyChanged += ViewStateMachine_PropertyChanged;
        }

        private void ViewStateMachine_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (!LoginSuccessful) return;
            if (!IsControllerListen) return;
            if (e.PropertyName == nameof(OItemSelected))
            {
                UnSetState(GlobalStateMachineState.ItemSelectRequest);
                if (OItemSelected == null)
                {
                    UnSetState(GlobalStateMachineState.ItemSelected);
                    if (OItemLoaded == null) return;

                    unloadItem?.Invoke(OItemLoaded);
                    return;
                }
                SetState(GlobalStateMachineState.ItemSelected);

                if (OItemLoaded == null)
                {
                    loadSelectedItem?.Invoke(OItemSelected);
                    return;
                }

                if (StateMachineType.HasFlag(StateMachineType.BlockLoadingSelected))
                {
                    if (OItemSelected.GUID == OItemLoaded.GUID)
                    {
                        return;
                    }
                }

                loadSelectedItem?.Invoke(OItemSelected);
            }

            if (e.PropertyName == nameof(LoginSuccessful))
            {
                SetState(GlobalStateMachineState.LoginDone);
                if (LoginSuccessful)
                {
                    loginSucceded?.Invoke();
                }
                else
                {
                    loginFailed?.Invoke();
                }
            }
        }

        public void LoginDone(bool success)
        {
            LoginSuccessful = success;
        }
    }
}
