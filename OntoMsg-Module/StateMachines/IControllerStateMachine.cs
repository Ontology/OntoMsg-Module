﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoMsg_Module.StateMachines
{
    public interface IControllerStateMachine
    {
        void OpenedSocket();
        void ClosedSocket();
        void ClosingSocket();
        void LoginDone(bool success);
    }
}
