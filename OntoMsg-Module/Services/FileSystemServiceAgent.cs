﻿using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoMsg_Module.Services
{
    public class FileSystemServiceAgent
    {

        private string webServiceUrl;
        public string BaseUrl { get; private set; }
        
        private string outputPath;// = @"C:\inetpub\wwwroot\OntologyModules\Resources";
        private string userPath;
        private string groupPath;
        private clsOntologyItem oItemUser;
        private clsOntologyItem oItemGroup;
        private string sessionId;
        private string sessionOutputPath;
        private object sessionLocker = new object();
        private List<SessionFile> sessionFiles = new List<SessionFile>();

        private clsLogStates logStates = new clsLogStates();

        public string SessionOutputPath
        {
            get { return sessionOutputPath; }
            private set
            {
                sessionOutputPath = value;
            }
        }

        public bool RemoveResource(string fileName)
        {
            var sessionFileFromList = sessionFiles.FirstOrDefault(sFile => sFile.FileName == fileName);

            if (sessionFileFromList == null) return true;

            return RemoveResource(sessionFileFromList);

        }

        public bool RemoveResource(SessionFile sessionFile)
        {
            var sessionFileFromList = sessionFiles.FirstOrDefault(sFile => sFile == sessionFile);

            if (sessionFileFromList != null)
            {
                sessionFileFromList.StreamWriter.Close();

                sessionFiles.Remove(sessionFileFromList);
            }


            if (!File.Exists(sessionFile.FilePath)) return true;
           
            try
            {
                File.Delete(sessionFile.FilePath);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }

        }

        public bool RemoveAllResources()
        {
            foreach(var sessionFile in sessionFiles)
            {
                try
                {
                    sessionFile.StreamWriter.Close();
                }
                catch(Exception ex)
                {

                }
                
            }

            

            if (!Directory.Exists(sessionOutputPath)) return true;

            sessionFiles.ForEach(sessionFile =>
            {

                try
                {

                    if (sessionFile.SessionFileType == SessionFileType.Stream)
                    {
                        File.Delete(sessionFile.FilePath);
                    }
                }
                catch (Exception ex)
                {

                }
            });
            sessionFiles = new List<SessionFile>();
            return true;
        }

        public SessionFile RequestUserFile(string idUser, string fileName, bool deleteFile)
        {
            var sessionFile = new SessionFile();
            sessionFile.StreamWriter = null;
            sessionFile.SessionFileType = SessionFileType.User;

            sessionFile.FileName = fileName;

            var userGroupOutputPath = ModuleDataExchanger.UserGroupPath +
                (ModuleDataExchanger.UserGroupPath.EndsWith(Path.DirectorySeparatorChar.ToString()) ? "" : Path.DirectorySeparatorChar.ToString()) +
                idUser;
            try
            {
                if (!Directory.Exists(userGroupOutputPath))
                {
                    Directory.CreateDirectory(userGroupOutputPath);
                }

                var filePath = userGroupOutputPath + Path.DirectorySeparatorChar + sessionFile.FileName;
                sessionFile.FilePath = filePath;

                sessionFile.Exists = File.Exists(sessionFile.FilePath);
                if (sessionFile.Exists && deleteFile)
                {
                    File.Delete(sessionFile.FilePath);
                }

                sessionFile.FileUri = GetFileUri(sessionFile.FileName, SessionFileType.User);

                lock(sessionLocker)
                {
                    sessionFiles.Add(sessionFile);
                }
                
                return sessionFile;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public Uri GetWriteStreamUrl(string sessionId, string fileName)
        {
            var url = webServiceUrl + "/" + sessionId + "/" + fileName;
            Uri uri;
            if (Uri.TryCreate(url, UriKind.Absolute, out uri))
            {
                return uri;
            }

            return null;
        }

        public SessionFile RequestWriteStream(string sessionId, string fileName)
        {
            SessionFile result = null;
            lock(sessionLocker)
            {
                var sessionFile = sessionFiles.FirstOrDefault(sessF => sessF.FileName == fileName);

                if (sessionFile == null)
                {
                    sessionFile = new SessionFile();
                    sessionFiles.Add(sessionFile);
                }

                sessionFile.FileName = fileName;

                this.sessionId = sessionId;
                sessionOutputPath = outputPath + Path.DirectorySeparatorChar + sessionId;
                try
                {
                    if (!Directory.Exists(sessionOutputPath))
                    {
                        Directory.CreateDirectory(sessionOutputPath);
                    }

                    var filePath = sessionOutputPath + Path.DirectorySeparatorChar + sessionFile.FileName;
                    sessionFile.FilePath = filePath;

                    try
                    {
                        if (File.Exists(sessionFile.FilePath))
                        {
                            File.Delete(sessionFile.FilePath);
                        }
                    }
                    catch (Exception)
                    {
                        filePath = sessionOutputPath + Path.DirectorySeparatorChar + "1" + sessionFile.FileName;
                        sessionFile.FilePath = filePath;

                    }
                    


                    var writeStream = new StreamWriter(sessionFile.FilePath);
                    sessionFile.StreamWriter = writeStream;

                    sessionFile.FileUri = GetFileUri(sessionFile.FileName, SessionFileType.Stream);
                    lock (sessionLocker)
                    {
                        sessionFiles.Add(sessionFile);
                    }
                    result = sessionFile;
                }
                catch (Exception ex)
                {
                    
                }
            }

            return result;
            

        }

        private Uri GetFileUri(string fileName, SessionFileType sessionFileType)
        {
            var url = "";
            if (sessionFileType == SessionFileType.Stream)
            {
                url = webServiceUrl + "/" + sessionId + "/" + fileName;
            }
            else if (sessionFileType == SessionFileType.User)
            {
                url = webServiceUrl + "/UserGroupRessources/" + oItemUser.GUID + "/" + fileName;
            }
            else if (sessionFileType == SessionFileType.Group)
            {
                url = webServiceUrl + "/UserGroupRessources/" + oItemGroup.GUID + "/" + fileName;
            }

            Uri uri;

            if (Uri.TryCreate(url, UriKind.Absolute, out uri))
            {
                return uri;
            }

            return null;
        }

        public clsOntologyItem DeleteSessionResources()
        {
            try
            {
                if (Directory.Exists(sessionOutputPath))
                {
                    Directory.Delete(sessionOutputPath, true);
                    
                }
                return logStates.LogState_Success.Clone();
            }
            catch(Exception ex)
            {
                return logStates.LogState_Error.Clone();
            }


        }

        public FileSystemServiceAgent(string webServiceUrl, string outputPath, clsOntologyItem oItemUser, clsOntologyItem oItemGroup)
        {
            this.BaseUrl = webServiceUrl;
            this.webServiceUrl = webServiceUrl + (webServiceUrl.EndsWith("/") ? "" : "/") + "Resources";
            this.outputPath = outputPath;
            userPath = ModuleDataExchanger.UserGroupPath + (ModuleDataExchanger.UserGroupPath.EndsWith(Path.DirectorySeparatorChar.ToString()) ? "" : Path.DirectorySeparatorChar.ToString()) + oItemUser.GUID;
            groupPath = ModuleDataExchanger.UserGroupPath + (ModuleDataExchanger.UserGroupPath.EndsWith(Path.DirectorySeparatorChar.ToString()) ? "" : Path.DirectorySeparatorChar.ToString()) + oItemGroup.GUID;
            this.oItemUser = oItemUser;
            this.oItemGroup = oItemGroup;
            if (!Directory.Exists(userPath))
            {
                Directory.CreateDirectory(userPath);
            }

            if (!Directory.Exists(groupPath))
            {
                Directory.CreateDirectory(groupPath);
            }
        }
    }
}
